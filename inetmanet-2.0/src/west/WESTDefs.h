/*
 * WESTDefs.h
 *
 *  Created on: Dec 15, 2014
 *      Author: diegovillamagna
 */

#ifndef WESTDEFS_H_
#define WESTDEFS_H_

// Packets transmission commands
#define  CMD_START "Cmd START"
#define  CMD_RANGING "Cmd RANGING"
#define  CMD_RANGING_RESPONSE "Cmd RANGING RESPONSE"
#define  CMD_RANGING_FINAL "Cmd RANGING FINAL"
#define  CMD_RANGING_REPORT "Cmd RANGING REPORT"
#define  CMD_DATA "Cmd DATA"
#define  CMD_INERTIAL "Cmd INERTIAL"


// Definitions of logging options
enum {
    LOG_MASK_TX             = 0,   // Record TX packets
    LOG_MASK_RX             = 1,   // Record RX packets
    LOG_MASK_RNGERR         = 2,   // Record No Responses events
    LOG_MASK_RNGNOTFOUND    = 3,   // Record No Neighbor Found for Ranging events
    LOG_MASK_RNGDUR         = 4,   // Record Duration Ranging between Open and Close Ranging process
    LOG_MASK_NODC           = 5,   // Record No T_ON constraints on Transmissions
    LOG_MASK_VALIDNEIGH     = 6,   // Record valid neighbors
    LOG_NUM_MASKS,
    LOG_MASK_BASE           = 0x0001,
    LOG_MASK_FULL           = 0xFFFF                // Record Everything
};

// Macro to build the mask
#define LOG_BUILD_MASK(mask)   (LOG_MASK_BASE << mask)

// Macro to check if log option is active
#define LOG_CHECK_OPTION(activeLogs,mask)   ((activeLogs & LOG_BUILD_MASK(mask)) ? true : false)

//// Payload Packet sizes
//enum
//{
//    // token included in the number of Bytes and header
//    DIM_KEEP_ALIVE = 19,
////    DIM_HELLO = 69,
//    DIM_HELLO = 65, // non considerando i 6 Byte degli ID e considerando i 2 Byte del RangingDone
////    DIM_RNG_POLL = 61,
//    DIM_RNG_POLL = 55, // non considerando i 6 Byte degli ID
////    DIM_RNG_RESPONSE = 64,
//    DIM_RNG_RESPONSE = 58, // non considerando i 6 Byte degli ID
//    DIM_RNG_FINAL_MSG = 27,
//    DIM_RNG_REPORT_MSG = 17,
//    DIM_NET_DATA = 127
//};
//
//// Packets duration
//enum
//{
//    DUR_KEEP_ALIVE = 3000,
//    DUR_HELLO = 7000,
//    DUR_RNG_POLL = 6500,
//    DUR_RNG_RESPONSE = 6900,
//    DUR_RNG_FINAL_MSG = 4800,
//    DUR_RNG_REPORT_MSG = 3800,
//    DUR_NET_DATA = 12000
//};

#endif /* WESTDEFS_H_ */

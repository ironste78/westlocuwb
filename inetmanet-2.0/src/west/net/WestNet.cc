//
// Copyright (C) 2001, 2003, 2004 Johnny Lai, Monash University, Melbourne, Australia
// Copyright (C) 2005 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include <iostream>

#include "WestNet.h"

#include "WestFrame_m.h"

#include "ModuleAccess.h"
#include "NodeOperations.h"

using std::cout;

Define_Module(WestNet);

WestNet::WestNet()
{

}

WestNet::~WestNet()
{

}

void WestNet::initialize(int stage)
{
    cSimpleModule::initialize(stage);

    if (stage == 0) {
        pingIn = findGate("pingIn");
        pingOut = findGate("pingOut");
        wlanIn = findGate("wlanIn");
        wlanOut = findGate("wlanOut");
    }

}

void WestNet::handleMessage(cMessage *msg)
{

        if (msg->getArrivalGateId() == pingIn){
            sendDown(msg);
        } else if (msg->getArrivalGateId() == wlanIn){
            sendUp(msg);
        }

}


void WestNet::sendDown(cMessage *msg)
{
    if (!strcmp(msg->getName(), CMD_START)) {
        send(msg, wlanOut);
    } else if (!strcmp(msg->getName(), CMD_INERTIAL)) {
        send(msg, wlanOut);
    } else {
        NetData *pkt = new NetData();
        send(pkt, wlanOut);
    }


}

void WestNet::sendUp(cMessage *msg)
{
    send(msg, pingOut);
}

void WestNet::finish()
{

}

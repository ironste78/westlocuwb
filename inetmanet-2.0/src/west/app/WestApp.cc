//
// Copyright (C) 2001, 2003, 2004 Johnny Lai, Monash University, Melbourne, Australia
// Copyright (C) 2005 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include <iostream>

#include "WestApp.h"

#include "ModuleAccess.h"
#include "NodeOperations.h"

using std::cout;

Define_Module(WestApp)
;

WestApp::WestApp() {
    nb = NULL;
    start = NULL;
    inertial = NULL;
    data = NULL;
}

WestApp::~WestApp() {
    cancelAndDelete(inertial);
    cancelAndDelete(start);
    cancelAndDelete(data);
}

void WestApp::initialize(int stage) {
    cSimpleModule::initialize(stage);
    if (stage == 0) {

        nb = NotificationBoardAccess().getIfExists();
        // notification board
        nb->subscribe(this, NF_SYNCHRONIZED);
        //nb->subscribe(this, NF_MAC_SYNCHRONIZED);

        synchronized = false;

        pingOut = findGate("pingOut");
        pingIn = findGate("pingIn");

        startTime = par ("startTime");

        start = new cMessage("start");

        inertial = new cMessage("inertial");

        data = new cMessage("data");

        timeGenerationInterval = getParentModule()->par(
                        "timeGenerationInterval");

        nodeID = getParentModule()->par("nodeID"); // node id = num Fix Host + 2
        if (nodeID == 1) {
            scheduleAt(simTime() + startTime, start);

        }

//        if (nodeID == 2) {
//            scheduleAt(simTime() + 10, data);
//        }

        scheduleAt(simTime() + timeGenerationInterval, inertial);

    }

}

void WestApp::receiveChangeNotification(int category, const cObject *details) {
    //if (category == NF_MAC_SYNCHRONIZED) {
    //    if (synchronized == false) {
    //        synchronized = true;
    //        scheduleAt(simTime() + timeGenerationInterval, inertial);
    //    }
    //}
}

void WestApp::handleMessage(cMessage *msg) {
    if (msg->isSelfMessage()) {
        handleSelfMessage(msg);
    } else {
        delete msg;
    }
}

void WestApp::handleSelfMessage(cMessage *msg) {
    if (msg == start) {
        nb->fireChangeNotification(NF_SYNCHRONIZED, start->dup());
        synchronized = true;
        msg->setName(CMD_START);
        sendDown(msg);
    } else {
        if (msg == data) {
            msg->setName(CMD_DATA);
            sendDown(msg);
        } else if (msg == inertial) {
            cMessage* newMsg = msg->dup();

            if (!inertial->isScheduled())
                scheduleAt(simTime() + timeGenerationInterval, inertial);

            newMsg->setName(CMD_INERTIAL);
            sendDown(newMsg);
            //delete newMsg;
        }
    }

}

void WestApp::sendDown(cMessage *msg) {
    send(msg, pingOut);
}

void WestApp::finish() {

}

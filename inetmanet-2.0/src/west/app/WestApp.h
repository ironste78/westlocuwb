//
// Copyright (C) 2001, 2003, 2004 Johnny Lai, Monash University, Melbourne, Australia
// Copyright (C) 2005 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include "INETDefs.h"

#include "IPvXAddress.h"

#include "NodeStatus.h"

#include "NotificationBoard.h"
#include "NotifierConsts.h"

#include "INotifiable.h"

#include "WESTDefs.h"


class PingPayload;

/**
 * Generates ping requests and calculates the packet loss and round trip
 * parameters of the replies.
 *
 * See NED file for detailed description of operation.
 */
class INET_API WestApp : public cSimpleModule, public INotifiable
{
  public:
    WestApp();
    virtual ~WestApp();

  protected:
    virtual void initialize(int stage);
    virtual void handleMessage(cMessage *msg);
    virtual void handleSelfMessage(cMessage *msg);
    virtual void receiveChangeNotification(int category, const cObject *details);
    virtual void sendDown(cMessage *msg);
    virtual void finish();

  protected:

    NotificationBoard *nb;

    /** gates */
    int pingIn;
    int pingOut;

    cMessage *msg;
    int nodeID;

    double startTime;

    bool synchronized;

    cMessage *sync;

    cMessage *start;
    cMessage *inertial;
    cMessage *data;

    simtime_t timeGenerationInterval;

};

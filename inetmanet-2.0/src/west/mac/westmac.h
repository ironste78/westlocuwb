//
// Copyright (C) 2013 OpenSim Ltd
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// author: Zoltan Bojthe
//

#ifndef __INET_WestMac_H
#define __INET_WestMac_H


#include "INETDefs.h"

#include "MACAddress.h"
#include "RadioState.h"
#include "WestFrame_m.h"
#include "WirelessMacBase.h"
#include "WESTDefs.h"

class IdealWirelessFrame;
class InterfaceEntry;
class IPassiveQueue;

// Definitions
#define BROADCAST_ADDRESS   0xFFFF

#define MAX_NODES   16
#define QUEUE_SIZE  5

// Un-comment to enable statistics' logging
#define ENABLE_LOG      1

#define DIM_LOG         2000
//#define DIM_LOG         5000

//#ifdef offsetof
//#undef offsetof
//#define offsetof(st, m) ((size_t)(&((st *)0)->m))
//#endif

typedef struct {
    bool isOpen;
    double timestamp;
} rngDuration;

typedef struct {
    int howMany;
    char name[20];
    double timestamp[DIM_LOG];
} westStats;

typedef struct {
    int value;
    double timestamp;
} basicWestStats;

typedef struct {
    int howMany;
    char name[20];
    basicWestStats stats[DIM_LOG];
} compoundWestStats;

typedef struct {
    /* TX side */
    westStats pktTX_KA;
    westStats pktTX_HELLO;
    westStats pktTX_RngPOLL;
    westStats pktTX_RngRESPONSE;
    westStats pktTX_RngFINAL;
    westStats pktTX_RngREPORT;
    westStats pktTX_NET;
    westStats pktTX_RngKA_NONEIGH;      // KA instead of Ranging - because of neighbor not found
    westStats pktTX_RngHELLO_NONEIGH;   // HELLO instead of Ranging - because of neighbor not found
    westStats pktTX_RngKA;              // KA instead of Ranging - because of not enough resource
    westStats pktTX_RngHELLO;           // HELLO instead of Ranging - because of not enough resource
    westStats noPktTX;                  // No transmission due to DC constraint
    westStats noRngRESPONSE;            // No response to a poll request due to DC constraint
    /* RX side */
    // Record just packets destined to the actual node
    westStats pktRX_RngPOLL;
    westStats pktRX_RngRESPONSE;
    westStats pktRX_RngFINAL;
    westStats pktRX_RngREPORT;
    westStats pktRX_NET;
    /* Trace valid neighbors */
    compoundWestStats validNeighbors;
    /* Ranging Duration */
    westStats rngDuration;
    westStats rngUnfinished;
} westStatsLog;


/**
 * Implements a MAC suitable for use with WestRadio.
 *
 * See the NED file for details.
 */
class INET_API WestMac : public WirelessMacBase, public cListener
{
  protected:
    static simsignal_t radioStateSignal;
    static simsignal_t dropPkNotForUsSignal;

    NotificationBoard *nb;


    bool DSTWR;   // DOUBLE (true) or SINGLE (false) SIDED TWO-WAY RANGING mode selector
    bool DC_MARGIN; // SPECIFIES IF DC-MARGIN IS USED

    // parameters
    int headerLength;       // IdealAirFrame header length in bytes
    double bitrate;         // [bits per sec]
    MACAddress address;     // MAC address [still needed because of the WirelessMacBase mechanism and interface]

    uint8_t packetCounter;  // sequence number

    IPassiveQueue *queueModule;
    cModule *radioModule;

    RadioState::State radioState;
    int outStandingRequests;
    simtime_t lastTransmitStartTime;
    cPacket *lastSentPk;
    simtime_t ackTimeout;
    cMessage *ackTimeoutMsg;
    cMessage *switchOff;
    cMessage *switchOn;

    //cMessage *macInertial;

    cMessage *timeSlotAlignment;
    uint8_t currentNode;

    cMessage *rangingTimeout;

    cMessage *dutyCycleReport;

    simtime_t KEEPduration;
    simtime_t DATAduration;
    simtime_t HELLOduration;
    simtime_t POLLduration;
    simtime_t RESPONSEduration;
    simtime_t FINALduration;
    simtime_t REPORTduration;

    // Incremental value for the T_ON to check the duty cycle constraint
    float t_on;

    // Durations for ranging to check the duty cycle constraint
    float transmissionLimit;    // [ms]
    float rangerLimit;          // [ms]
    float responderLimit;       // [ms]
    float dc_margin;            // [ms]

    int nodeID;
    uint8_t numNodes;

    bool synchronized;
    bool nodeOff; // debug scope


    // WEST Frames
    WestFrame wstFrame;

    // Timeslot duration for the SPIN
    simtime_t timeSlot;

    // Inter-arrival time of the INERTIAL message - for recording statistics
    simtime_t timeGenerationInterval;

//    int maxNeighborsPortable; // massimo numero di nodi trasportabili nei pacchetti di ranging POLL/RESPONSE

    // Invariable structures
    FrameCtrl frmCtrl;

    typedef struct  {
        bool isPresent;
        double epoch;
        float T_ON;
        uint8_t rangingDone;
        double lastRanging;
        uint16_t distance[MAX_NODES];
    } NeighborsList;

    NeighborsList nl[MAX_NODES];

    // Flag to enable ranging (after the start and the first KA loop)
    bool rangingToBeDone;

    uint16_t PANcoordinator;
    uint16_t pktEpoch;

    // **************************************************
    //              STATISTICS VARIABLES
    // **************************************************

    // Logging option (defined in INI file, as referred in WESTDefs.h)
    uint16_t logOption;

    // Counter of the neighbors with which ranging has been done
    int neighborsRangingDone;

    //int numeroViciniTre;
    //int numeroViciniQuattro;
    //int numeroViciniCinque;

    //int compNL_three;
    //int compNL_four;
    //int compNL_five;

    //int countInertialTimes;

    //double tempoTre_array[DIM_LOG];
    //double tempoQuattro_array[DIM_LOG];
    //double tempoCinque_array[DIM_LOG];

    //double inertialTimes[DIM_LOG];
    //uint8_t numNeighbors[DIM_LOG];

    char fileName[50];      // the global file name
    //char fileNameTwo[50];
    FILE *file;
    //FILE *fileTwo;

    //int countNeighbors;

    //int countTransmissionFailures; // contatore di trasmissioni fallite per mancanza di risorse temporali

#ifdef ENABLE_LOG
    westStatsLog westLog;
#endif
    rngDuration rngDur[MAX_NODES];

  protected:
    /** implements MacBase functions */
    //@{
    virtual void flushQueue();
    virtual void clearQueue();
    virtual InterfaceEntry *createInterfaceEntry();
    //@}

    virtual void startTransmitting(cPacket *msg);
    //virtual bool dropFrameNotForUs(IdealWirelessFrame *frame);
    virtual IdealWirelessFrame *encapsulate(cPacket *msg);
    virtual cPacket *decapsulate(IdealWirelessFrame *frame);
    virtual void initializeMACAddress();
    virtual void acked(IdealWirelessFrame *frame);   // called by other IdealMac module, when receiving a packet with my moduleID

    // get MSG from queue
    virtual void getNextMsgFromHL();

    //cListener:
    virtual void receiveSignal(cComponent *src, simsignal_t id, long x);

    virtual void receiveChangeNotification(int category, const cObject *details);

    /** implements WirelessMacBase functions */
    //@{
    virtual void handleSelfMsg(cMessage *msg);
    virtual void handleUpperMsg(cPacket *msg);
    virtual void handleCommand(cMessage *msg);
    virtual void handleLowerMsg(cPacket *msg);
    //@}


    /** implements new functions for the WEST model */
    //@{
    virtual uint8_t handleTransmission(cMessage *msg, uint16_t srcId);

    virtual uint16_t computePayloadSize(uint8_t typeCode);
    virtual simtime_t computeWestDuration(uint8_t typeCode);
    virtual simtime_t computeDuration(int payloadSize);
    virtual WestFrame *buildWestFrame(uint8_t typeCode, uint16_t dst);
    //@}

  public:
    WestMac();
    virtual ~WestMac();

  protected:
    virtual int numInitStages() const { return 2; }
    virtual void initialize(int stage);
    virtual void finish();

    cPacket *transmissionQueue[QUEUE_SIZE];
    uint8_t queueIt;

    /** @name Different tracked statistics.*/
    double dutyCycle; //microsecondi

    /** @brief Records general statistics?*/
    bool stats;

};

#endif

//
// Copyright (C) 2013 OpenSim Ltd
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// author: Zoltan Bojthe
//

#include <stdio.h>
#include <string.h>
//#include <stddef.h>     /* offsetof */

#include "westmac.h"

#include "WestRadio.h"
#include "IdealWirelessFrame_m.h"
#include "Ieee802Ctrl_m.h"
#include "IInterfaceTable.h"
#include "InterfaceTableAccess.h"
#include "IPassiveQueue.h"
#include "opp_utils.h"

#include "mac_enum.h"

Define_Module(WestMac);

simsignal_t WestMac::radioStateSignal = registerSignal("radioState");
simsignal_t WestMac::dropPkNotForUsSignal = registerSignal("dropPkNotForUs");

WestMac::WestMac() {
    nb = NULL;
    queueModule = NULL;
    radioModule = NULL;
    lastSentPk = NULL;
    ackTimeoutMsg = NULL;
    timeSlotAlignment = NULL;
    rangingTimeout = NULL;
    switchOff = NULL;
    switchOn = NULL;
    dutyCycleReport = NULL;
    //macInertial = NULL;
}

WestMac::~WestMac() {
    delete lastSentPk;
    delete CMD_DATA;
    delete CMD_INERTIAL;
    delete CMD_RANGING;
    delete CMD_RANGING_FINAL;
    delete CMD_RANGING_REPORT;
    delete CMD_RANGING_RESPONSE;
    delete CMD_START;
    cancelAndDelete(ackTimeoutMsg);

    cancelAndDelete(timeSlotAlignment);
    cancelAndDelete(rangingTimeout);
    cancelAndDelete(switchOff);
    cancelAndDelete(switchOn);
    cancelAndDelete(dutyCycleReport);

    //cancelAndDelete(macInertial);
}

void WestMac::flushQueue() {
    ASSERT(queueModule);
    while (!queueModule->isEmpty()) {
        cMessage *msg = queueModule->pop();
        //TODO emit(dropPkIfaceDownSignal, msg); -- 'pkDropped' signals are missing in this module!
        delete msg;
    }
    queueModule->clear(); // clear request count
}

void WestMac::clearQueue() {
    ASSERT(queueModule);
    queueModule->clear();
}

void WestMac::initialize(int stage) {
    WirelessMacBase::initialize(stage);

    // all initialization is done in the first stage
    if (stage == 0) {
        // notification board
        nb = NotificationBoardAccess().getIfExists();
        nb->subscribe(this, NF_SYNCHRONIZED);
        nb->subscribe(this, NF_MAC_SYNCHRONIZED);

        //compNL_three = 0;
        //compNL_four = 0;
        //compNL_five = 0;

        //countInertialTimes = 0;

        //memset(tempoTre_array, 0, DIM_LOG * sizeof(double));
        //memset(tempoQuattro_array, 0, DIM_LOG * sizeof(double));
        //memset(tempoCinque_array, 0, DIM_LOG * sizeof(double));

        //memset(inertialTimes, 0, DIM_LOG * sizeof(double));
        //memset(numNeighbors, 0, DIM_LOG * sizeof(uint8_t));

        //numeroViciniTre = 3;
        //numeroViciniQuattro = 4;
        //numeroViciniCinque = 5;

        //countNeighbors = 5;

        //countTransmissionFailures = 0;

#ifdef ENABLE_LOG
        memset(&westLog, 0, sizeof(westStatsLog));
#endif
        memset(rngDur, 0, MAX_NODES * sizeof(rngDuration));

        neighborsRangingDone = 0;

        outStandingRequests = 0;
        lastTransmitStartTime = -1.0;

        packetCounter = 0;

        t_on = 0;

        dutyCycle = 0;

        pktEpoch = 0;

        DSTWR = getParentModule()->getParentModule()->getParentModule()->par("DSTWR");
        DC_MARGIN = getParentModule()->getParentModule()->getParentModule()->par("DC_MARGIN");

        bitrate = par("bitrate").doubleValue();
        headerLength = par("headerLength").longValue();
        ackTimeout = par("ackTimeout");

        radioModule = gate("lowerLayerOut")->getPathEndGate()->getOwnerModule();
        WestRadio *irm = check_and_cast<WestRadio *>(radioModule);
        irm->subscribe(radioStateSignal, this);

        // find queueModule
        cGate *queueOut = gate("upperLayerIn")->getPathStartGate();
        queueModule = dynamic_cast<IPassiveQueue *>(queueOut->getOwnerModule());
        if (!queueModule)
            throw cRuntimeError("Missing queueModule");

        initializeMACAddress();

        ackTimeoutMsg = new cMessage("link-break");

        // register our interface entry in IInterfaceTable
        registerInterface();
        nb = NotificationBoardAccess().get();

        synchronized = false;
        nodeOff = false;

        // construct frame control field
        frmCtrl.frmType = Ieee802154_DATA;      //data type
        frmCtrl.secu = false;
        frmCtrl.frmPending = false;
        frmCtrl.intraPan = true;
        frmCtrl.dstAddrMode = defFrmCtrl_AddrMode16;
        frmCtrl.srcAddrMode = defFrmCtrl_AddrMode16;

        // Packets durations [ms]
        KEEPduration = computeWestDuration(PKT_KEEP_ALIVE);
        DATAduration = computeWestDuration(PKT_NET_DATA);
        HELLOduration = computeWestDuration(PKT_HELLO);
        POLLduration = computeWestDuration(PKT_RNG_POLL);
        RESPONSEduration = computeWestDuration(PKT_RNG_RESPONSE);
        FINALduration = computeWestDuration(PKT_RNG_FINAL_MSG);
        REPORTduration = computeWestDuration(PKT_RNG_REPORT_MSG);

        // Number of nodes
        numNodes = getParentModule()->getParentModule()->getParentModule()->par("numFixHosts");

        // Node ID
        nodeID = getParentModule()->getParentModule()->par("nodeID");
        if (nodeID < 0)
            error("Undefined nodeID for the current node!!");

        // Timeslot Alignment Message to implement the timer
        timeSlotAlignment = new cMessage("timeSlotAlignment");

        timeGenerationInterval = getParentModule()->getParentModule()->par(
                "timeGenerationInterval");

        // Log option
        logOption = getParentModule()->getParentModule()->getParentModule()->par("logOption");

        // Current transmitting node
        currentNode = 0xFF;

        if (DSTWR) {
            // DOUBLE SIDED TWO-WAY RANGING

            timeSlot = par("timeSlot");

            // Minimum resource for starting a ranging process
            rangerLimit = POLLduration.dbl() + FINALduration.dbl();

            // Minimum resource for responding to a ranging process request
            responderLimit = RESPONSEduration.dbl() + REPORTduration.dbl();
        } else {
            // SINGLE SIDED TWO-WAY RANGING

            timeSlot = par("timeSlotSingleWay");

            // Minimum resource for starting a ranging process
            rangerLimit = POLLduration.dbl();

            // Minimum resource for responding to a ranging process request
            responderLimit = RESPONSEduration.dbl();
        }

        // Margin to the duty cycle constraint to be reserved for network synchronization
        // i.e., sending at least a KeepAlive packet each timeslot
        dc_margin = 1 / (numNodes * timeSlot.dbl() * 1000) * KEEPduration.dbl();

        // Max value of the transmission time for a node [ms]
        if (DC_MARGIN) {
            transmissionLimit = 50 - dc_margin * 1000;
        } else {
            // Ignore the DC margin
            transmissionLimit = 50;
        }

        // Ranging timeout to send responses
        rangingTimeout = new cMessage("rangingTimeout");

        switchOff = new cMessage("switchOff");

        switchOn = new cMessage("switchOn");

        dutyCycleReport = new cMessage("dutyCycleReport");

        //macInertial = new cMessage("macInertial");

        scheduleAt(simTime() + 1, dutyCycleReport);

        //scheduleAt(simTime() + 1 + timeGenerationInterval, macInertial);

        memset(nl, 0, MAX_NODES * sizeof(NeighborsList));

        rangingToBeDone = false;

        nl[nodeID].isPresent = true;

        PANcoordinator = 0xFFFF;

        // simula lo spegnimento di un nodo
        // if (nodeID == 3) {
        //  scheduleAt(simTime() + 20, switchOff);
        //  scheduleAt(simTime() + 70, switchOn);
        // }

        //initialize tranmissionQueue
        for (int i = 0; i < QUEUE_SIZE; i++) {
            transmissionQueue[i] = NULL;
        }

        queueIt = 0;


        sprintf(fileName, "Stats");

        char ndID[5];
        char ds[5];
        char dc[5];
        char slot_duration[5];

        sprintf(ndID, "%02d", nodeID);

        if (DSTWR) {
            sprintf(ds, "DS");
        } else {
            sprintf(ds, "NDS");
        }

        if (DC_MARGIN) {
            sprintf(dc, "DC");
        } else {
            sprintf(dc, "NDC");
        }

        double timeslotinms = timeSlot.dbl() * 1000;
        sprintf(slot_duration, "%.f", timeslotinms);

        strcat(fileName, ndID);
        strcat(fileName, "_");
        strcat(fileName, ds);
        strcat(fileName, "_");
        strcat(fileName, dc);
        strcat(fileName, "_");
        strcat(fileName, slot_duration);
        strcat(fileName, ".csv");

        file = fopen(fileName, "w");
        if (file == NULL)
            throw cRuntimeError("Error trying to open the output file!");

        // Insert the values' header
        fputs("%StatCode;", file);
        fputs("HowMany;", file);
        fputs("Values", file);
        fputs("\n", file);
        fclose(file);

#if 0
        sprintf(fileNameTwo, "Vicini");

        strcat(fileNameTwo, "_");
        strcat(fileNameTwo, ds);
        strcat(fileNameTwo, "_");
        strcat(fileNameTwo, dc);
        strcat(fileNameTwo, "_");
        strcat(fileNameTwo, slot_duration);
        strcat(fileNameTwo, ".csv");

        if (nodeID == 1) {
            fileTwo = fopen(fileNameTwo, "w");
            fputs("%NODE_ID;", fileTwo);
            fputs("SIM_time;", fileTwo);
            fputs("N_VICINI", fileTwo);
            fputs("\n", fileTwo);
            fclose(fileTwo);
        }
#endif

    }
}

void WestMac::initializeMACAddress() {
    const char *addrstr = par("address");

    if (!strcmp(addrstr, "auto")) {
        // assign automatic address
        address = MACAddress::generateAutoAddress();

        // change module parameter from "auto" to concrete address
        par("address").setStringValue(address.str().c_str());
    } else {
        address.setAddress(addrstr);
    }
}

InterfaceEntry *WestMac::createInterfaceEntry() {
    InterfaceEntry *e = new InterfaceEntry(this);

    // interface name: NIC module's name without special characters ([])
    e->setName(
            OPP_Global::stripnonalnum(getParentModule()->getFullName()).c_str());

    // data rate
    e->setDatarate(bitrate);

    // generate a link-layer address to be used as interface token for IPv6
    e->setMACAddress(address);
    e->setInterfaceToken(address.formInterfaceIdentifier());

    // MTU: typical values are 576 (Internet de facto), 1500 (Ethernet-friendly),
    // 4000 (on some point-to-point links), 4470 (Cisco routers default, FDDI compatible)
    e->setMtu(par("mtu").longValue());

    // capabilities
    e->setMulticast(true);
    e->setBroadcast(true);

    return e;
}

void WestMac::receiveSignal(cComponent *src, simsignal_t id, long x) {
    if (id == radioStateSignal && src == radioModule) {
        radioState = (RadioState::State) x;
        if (radioState != RadioState::TRANSMIT && !lastSentPk)
            getNextMsgFromHL();
    }
}

void WestMac::startTransmitting(cPacket *msg) {
    // if there's any control info, remove it; then encapsulate the packet
    //    if (lastSentPk)
    //        throw cRuntimeError("Model error: unacked send");
    //    Ieee802Ctrl *ctrl = check_and_cast<Ieee802Ctrl*>(msg->getControlInfo());
    //    MACAddress dest = ctrl->getDest();
    //    if (!dest.isBroadcast() && !dest.isMulticast() && !dest.isUnspecified())
    //    {   // unicast
    //        lastSentPk = msg->dup();
    //        lastSentPk->setControlInfo(ctrl->dup());
    //        scheduleAt(simTime() + ackTimeout, ackTimeoutMsg);
    //    }

    dutyCycle += msg->getDuration().dbl();

    IdealWirelessFrame *frame = encapsulate(msg);

    // send

    if (synchronized) {
        EV<< "Starting transmission of " << frame << endl;
        frame->setDuration(msg->getDuration());
        sendDown(frame);
    } else {
        EV << "Cannot transmit, node " << nodeID << " is not synchronized" << endl;
        delete frame;
    }

}

void WestMac::getNextMsgFromHL() {
    ASSERT(outStandingRequests >= queueModule->getNumPendingRequests());
    if (outStandingRequests == 0 && lastTransmitStartTime < simTime()) {
        queueModule->requestPacket();
        outStandingRequests++;
    }
    ASSERT(outStandingRequests <= 1);
}

void WestMac::handleUpperMsg(cPacket *msg) {
    outStandingRequests--;
    if (radioState == RadioState::TRANSMIT) {
        // Logic error: we do not request packet from the external queue when radio is transmitting
        error("Received msg for transmission but transmitter is busy");
    } else if (radioState == RadioState::SLEEP) {
        EV<< "Dropped upper layer message " << msg << " because radio is SLEEPing.\n";
    }
    else
    {
        // We are idle, so we can start transmitting right away.
        EV << "Received " << msg << " for transmission\n";
        //startTransmitting(msg);
        if (queueIt < QUEUE_SIZE - 1) {
            transmissionQueue[queueIt] = msg;
            queueIt++;
        } else {
            delete msg;
            throw cRuntimeError("transmissionQueue is full");
        }

    }
}

void WestMac::handleCommand(cMessage *msg) {
    //error("Unexpected command received from higher layer");
    if (!strcmp(msg->getName(), CMD_START)) {

        // Immaginiamo che chi inizia è il PAN coordinator della rete
        PANcoordinator = nodeID;

        handleTransmission(msg, 0);
        goto hC_end;

    }

    if (!strcmp(msg->getName(), CMD_INERTIAL)) {
        // ********************************************
        //           RECORD NUM RANGING NEIGHBORS
        // ********************************************

        //if (!macInertial->isScheduled())
        //    scheduleAt(simTime() + timeGenerationInterval, macInertial);

        neighborsRangingDone = 0;

        simtime_t currentTime = simTime();
        for (int i = 0; i < MAX_NODES; i++) {
            if (nl[i].rangingDone == RNG_DONE
                    && nl[i].lastRanging >= (currentTime.dbl() - 0.3))
                neighborsRangingDone++;

            //if (nl[i].rangingDone == RNG_PENDING){
            //    throw cRuntimeError("some ranging pending remaining!!");
            //}
        }

#ifdef ENABLE_LOG
        if (LOG_CHECK_OPTION(logOption,LOG_MASK_VALIDNEIGH)) {
            // Record how many valid neighbors
            westLog.validNeighbors.stats[(westLog.validNeighbors.howMany) % DIM_LOG].value = neighborsRangingDone;
            westLog.validNeighbors.stats[(westLog.validNeighbors.howMany) % DIM_LOG].timestamp = currentTime.dbl();
            westLog.validNeighbors.howMany++;
        }
#endif

        // Reset of rangingDone when lastRanging greater than 300 ms
        for (int j = 0; j < numNodes; j++) {
            if (nl[j].rangingDone != RNG_UNSET
                    && (currentTime - nl[j].lastRanging) > timeGenerationInterval){

                // Reset Ranging Done
                nl[j].rangingDone = RNG_UNSET;

                if (rngDur[j].isOpen){
                    // Reset ranging open and record unfinished ranging processes
                    rngDur[j].isOpen = false;
#ifdef ENABLE_LOG
                    if (LOG_CHECK_OPTION(logOption,LOG_MASK_RNGDUR)) {
                        westLog.rngUnfinished.timestamp[(westLog.rngUnfinished.howMany++) % DIM_LOG] = currentTime.dbl();
                    }
#endif
                }
            }
        }

        goto hC_end;
    }

    hC_end: delete msg;

}

void WestMac::handleLowerMsg(cPacket *msg) {
    IdealWirelessFrame *frame = check_and_cast<IdealWirelessFrame *>(msg);
    if (frame->hasBitError()) {
        EV<< "Bit error in " << frame << ", discarding" << endl;
        delete frame;
        return;
    }

    // Record current time
    simtime_t currentRxTime = simTime();

    // Extract the IEEE802.15.4 (West) frame
    WestFrame *westMsg = check_and_cast<WestFrame *>(frame->decapsulate());

    // Get the source node ID
    uint16_t sourceID = westMsg->getSrcAddr();

    if (sourceID < MAX_NODES) {
        nl[sourceID].isPresent = true;
    } else {
        throw cRuntimeError("Invalid neighbor id");
    }

    if (!nodeOff) {
        synchronized = true;
        cMessage* macSync = new cMessage();
        nb->fireChangeNotification(NF_MAC_SYNCHRONIZED, macSync);
        delete macSync;
    } else {
        char nOff[10];
        sprintf(nOff, "NODE OFF");
        getParentModule()->getParentModule()->getDisplayString().setTagArg("t",
                0, nOff);
        getParentModule()->getParentModule()->getDisplayString().setTagArg("i",
                0, "status/stop");
    }

    uint8_t tokenInfo = 0;

    // Extract and save the current PANcoordinator, if valid or not done yet
    if (PANcoordinator == 0xFFFF || westMsg->getDstPanId() != 0xFFFF)
        PANcoordinator = westMsg->getDstPanId();

    // Check which type of frame we have received

    // ********************************************
    //              KEEP ALIVE PACKET
    // ********************************************
    if (westMsg->getFunctionCode() == PKT_KEEP_ALIVE) {
        // Parse the encapsulated packet accordingly
        KeepAlive *pkt = check_and_cast<KeepAlive *>(
                westMsg->getEncapsulatedPacket());

        // Get the information about the token and remaining timing
        tokenInfo = pkt->getToken();

        nl[sourceID].epoch = currentRxTime.dbl();
        nl[sourceID].T_ON = westMsg->getT_on() / 5;

        if ((tokenInfo & TOKEN_CODE) == TOKEN_CODE) {
            // Cancel a previously scheduled timer (if any)
            if (timeSlotAlignment->isScheduled())
                cancelEvent(timeSlotAlignment);

            // The source node is carrying the token
            currentNode = sourceID;
            // Compute the timing for the next schedule
            double nextTimeout = (double) (tokenInfo & ~TOKEN_CODE) / 1000;
            scheduleAt(currentRxTime + nextTimeout - KEEPduration.dbl() / 1000,
                    timeSlotAlignment);
        }

        // Jump to the end of the function
        goto goodEnd;
    }

    // ********************************************
    //              HELLO PACKET
    // ********************************************
    if (westMsg->getFunctionCode() == PKT_HELLO) {
        // Parse the encapsulated packet accordingly
        HelloPkt *pkt = check_and_cast<HelloPkt *>(
                westMsg->getEncapsulatedPacket());

        // Get the information about the token and remaining timing
        tokenInfo = pkt->getToken();

        uint16_t rangingDone_ = pkt->getFullPayload().rangingDone;

        uint16_t rDoneCpy = rangingDone_;

        // Reset the old information of the distances computed from this neighbor
        memset(nl[sourceID].distance, 0, MAX_NODES * sizeof(uint16_t));

        uint8_t j = 0;
        uint8_t i = 0;
        if (rDoneCpy != 0){
            // Get the new values of the distances computed by this node
            while (rDoneCpy != 0) {
                uint16_t MASK = (1 << i);

                if (rDoneCpy & MASK){
                    nl[sourceID].distance[i] = pkt->getFullPayload().distance[j++];

                    rDoneCpy &= (~MASK);   // Clear that bit
                }
                i++;
            }
        }

        // Safety model check
        if (j > maxNeighborsPortable)
            throw cRuntimeError("MODEL ERROR: Too many valid distance with neighbors");

        nl[sourceID].epoch = currentRxTime.dbl();
        nl[sourceID].T_ON = westMsg->getT_on() / 5;

        if ((tokenInfo & TOKEN_CODE) == TOKEN_CODE) {
            // Cancel a previously scheduled timer (if any)
            if (timeSlotAlignment->isScheduled())
                cancelEvent(timeSlotAlignment);

            // The source node is carrying the token
            currentNode = sourceID;

            // Compute the timing for the next schedule
            double nextTimeout = (double) (tokenInfo & ~TOKEN_CODE) / 1000;
            scheduleAt(currentRxTime + nextTimeout - HELLOduration.dbl() / 1000,
                    timeSlotAlignment);
        }


        // Check if a pending ranging is solved by this packet
        if (nl[sourceID].rangingDone == RNG_PENDING
                && (rangingDone_ & (1 << nodeID)) ){
            nl[sourceID].rangingDone = RNG_DONE;
            // The value of the distance has been already saved from this packet in the above loop
            // and is found in nl[sourceID].distance[nodeID].

            // Record closure of ranging process if it was still open with node sourceID ...
            if (rngDur[sourceID].isOpen) {
                rngDur[sourceID].isOpen = false;
#ifdef ENABLE_LOG
                if (LOG_CHECK_OPTION(logOption,LOG_MASK_RNGDUR)) {
                    // ... and compute duration
                    westLog.rngDuration.timestamp[(westLog.rngDuration.howMany++) % DIM_LOG] =
                            currentRxTime.dbl() - rngDur[sourceID].timestamp;
                }
#endif
                // Reset timestamp
                rngDur[sourceID].timestamp = 0;
            }
        }

        // Jump to the end of the function
        goto goodEnd;
    }

    // ********************************************
    //              RANGING POLL PACKET
    // ********************************************
    if (westMsg->getFunctionCode() == PKT_RNG_POLL) {
        // Parse the encapsulated packet accordingly
        RangingPoll *pkt = check_and_cast<RangingPoll *>(
                westMsg->getEncapsulatedPacket());

        //double time = simTime().dbl();

        // Get the information about the token and remaining timing
        tokenInfo = pkt->getToken();

        uint16_t rangingDone_ = pkt->getFullPayload().rangingDone;

        uint16_t rDoneCpy = rangingDone_;

        // Reset the old information of the distances computed from this neighbor
        memset(nl[sourceID].distance, 0, MAX_NODES * sizeof(uint16_t));

        uint8_t j = 0;
        uint8_t i = 0;
        if (rDoneCpy != 0){
            // Get the new values of the distances computed by this node
            while (rDoneCpy != 0) {
                uint16_t MASK = (1 << i);

                if (rDoneCpy & MASK){
                    nl[sourceID].distance[i] = pkt->getFullPayload().distance[j++];

                    rDoneCpy &= (~MASK);   // Clear that bit
                }
                i++;
            }
        }

        // Safety model check
        if (j > maxNeighborsPortable)
            throw cRuntimeError("MODEL ERROR: Too many valid distance with neighbors");

        nl[sourceID].epoch = currentRxTime.dbl();
        nl[sourceID].T_ON = westMsg->getT_on() / 5;

        if ((tokenInfo & TOKEN_CODE) == TOKEN_CODE) {
            // Cancel a previously scheduled timer (if any)
            if (timeSlotAlignment->isScheduled())
                cancelEvent(timeSlotAlignment);

            // The source node is carrying the token
            currentNode = sourceID;

            // Compute the timing for the next schedule
            double nextTimeout = (double) (tokenInfo & ~TOKEN_CODE) / 1000;
            scheduleAt(currentRxTime + nextTimeout - POLLduration.dbl() / 1000,
                    timeSlotAlignment);
        }

        // Check if a pending ranging is solved by this packet
        if (nl[sourceID].rangingDone == RNG_PENDING
                && (rangingDone_ & (1 << nodeID)) ){
            nl[sourceID].rangingDone = RNG_DONE;
            // The value of the distance has been already saved from this packet in the above loop
            // and is found in nl[sourceID].distance[nodeID].

            // Record closure of ranging process if it was still open with node sourceID ...
            if (rngDur[sourceID].isOpen) {
                rngDur[sourceID].isOpen = false;
#ifdef ENABLE_LOG
                if (LOG_CHECK_OPTION(logOption,LOG_MASK_RNGDUR)) {
                    // ... and compute duration
                    westLog.rngDuration.timestamp[(westLog.rngDuration.howMany++) % DIM_LOG] =
                            currentRxTime.dbl() - rngDur[sourceID].timestamp;
                }
#endif
                // Reset timestamp
                rngDur[sourceID].timestamp = 0;
            }
        }


        // If the packet is for me, activate a response to this ranging request
        if (westMsg->getDstAddr() == nodeID
                || westMsg->getDstAddr() == BROADCAST_ADDRESS) {
#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_RX)) {
                // Record this reception
                westLog.pktRX_RngPOLL.timestamp[(westLog.pktRX_RngPOLL.howMany++) % DIM_LOG] = currentRxTime.dbl();
            }
#endif

            // Record open of ranging process with node sourceID
            rngDur[sourceID].isOpen = true;
            rngDur[sourceID].timestamp = currentRxTime.dbl();


            getParentModule()->getParentModule()->bubble(
                    "RANGING POLL RICEVUTO");
            cMessage *resp = new cMessage();
            resp->setName(CMD_RANGING_RESPONSE);
            handleTransmission(resp, sourceID);
        }

        // Jump to the end of the function
        goto goodEnd;
    }

    // ********************************************
    //           RANGING RESPONSE PACKET
    // ********************************************
    if (westMsg->getFunctionCode() == PKT_RNG_RESPONSE) {
        // Parse the encapsulated packet accordingly
        RangingResponse *pkt = check_and_cast<RangingResponse *>(
                westMsg->getEncapsulatedPacket());

        // Get the information about the token and remaining timing
        tokenInfo = pkt->getToken();

        uint16_t rangingDone_ = pkt->getFullPayload().rangingDone;

        uint16_t rDoneCpy = rangingDone_;

        // Reset the old information of the distances computed from this neighbor
        memset(nl[sourceID].distance, 0, MAX_NODES * sizeof(uint16_t));

        uint8_t j = 0;
        uint8_t i = 0;
        if (rDoneCpy != 0){
            // Get the new values of the distances computed by this node
            while (rDoneCpy != 0) {
                uint16_t MASK = (1 << i);

                if (rDoneCpy & MASK){
                    nl[sourceID].distance[i] = pkt->getFullPayload().distance[j++];

                    rDoneCpy &= (~MASK);   // Clear that bit
                }
                i++;
            }
        }

        // Safety model check
        if (j > maxNeighborsPortable)
            throw cRuntimeError("MODEL ERROR: Too many valid distance with neighbors");

        nl[sourceID].epoch = currentRxTime.dbl();
        nl[sourceID].T_ON = westMsg->getT_on() / 5;
/*
        if ((tokenInfo & TOKEN_CODE) == TOKEN_CODE) {
            // Cancel a previously scheduled timer (if any)
            if (timeSlotAlignment->isScheduled())
                cancelEvent(timeSlotAlignment);
        }
*/

        // If the packet is for me, activate a follow on to this ranging request
        if (westMsg->getDstAddr() == nodeID
                || westMsg->getDstAddr() == BROADCAST_ADDRESS) {
#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_RX)) {
                // Record this reception
                westLog.pktRX_RngRESPONSE.timestamp[(westLog.pktRX_RngRESPONSE.howMany++) % DIM_LOG] = currentRxTime.dbl();
            }
#endif

            getParentModule()->getParentModule()->bubble(
                    "RANGING RESPONSE RICEVUTO");

            if (DSTWR) {
                cMessage *resp = new cMessage();
                resp->setName(CMD_RANGING_FINAL);
                handleTransmission(resp, sourceID);
            } else {
                // In SINGLE SIDED TWO-WAY RANGING mode, at this moment the node knows
                // the value of the distance

                // Close the ranging procedure
                nl[sourceID].rangingDone = RNG_DONE;
                nl[sourceID].lastRanging = currentRxTime.dbl();

                // For testing and debugging mode, set the value of the distance to be traceable, as:
                nl[sourceID].distance[nodeID] = ((nodeID & 0x00FF) << 8)
                                        | (sourceID & 0x00FF);

                // Record closure of ranging process if it was still open with node sourceID ...
                if (rngDur[sourceID].isOpen) {
                    rngDur[sourceID].isOpen = false;
#ifdef ENABLE_LOG
                    if (LOG_CHECK_OPTION(logOption,LOG_MASK_RNGDUR)) {
                        // ... and compute duration
                        westLog.rngDuration.timestamp[(westLog.rngDuration.howMany++) % DIM_LOG] =
                                currentRxTime.dbl() - rngDur[sourceID].timestamp;
                    }
#endif
                    // Reset timestamp
                    rngDur[sourceID].timestamp = 0;
                }
            }
        } else {
            // Packet not for me. Exploit overhearing.

            // Check if a pending ranging is solved by this packet
            if (nl[sourceID].rangingDone == RNG_PENDING
                    && (rangingDone_ & (1 << nodeID)) ){
                nl[sourceID].rangingDone = RNG_DONE;
                // The value of the distance has been already saved from this packet in the above loop
                // and is found in nl[sourceID].distance[nodeID].

                // Record closure of ranging process if it was still open with node sourceID ...
                if (rngDur[sourceID].isOpen) {
                    rngDur[sourceID].isOpen = false;
#ifdef ENABLE_LOG
                    if (LOG_CHECK_OPTION(logOption,LOG_MASK_RNGDUR)) {
                        // ... and compute duration
                        westLog.rngDuration.timestamp[(westLog.rngDuration.howMany++) % DIM_LOG] =
                                currentRxTime.dbl() - rngDur[sourceID].timestamp;
                    }
#endif
                    // Reset timestamp
                    rngDur[sourceID].timestamp = 0;
                }
            }
        }

        // Jump to the end of the function
        goto goodEnd;
    }

    // ********************************************
    //           RANGING FINAL PACKET
    // ********************************************
    if (westMsg->getFunctionCode() == PKT_RNG_FINAL_MSG) {
        // Parse the encapsulated packet accordingly
        //RangingFinal *pkt = check_and_cast<RangingFinal *>(
        //                westMsg->getEncapsulatedPacket());

        nl[sourceID].epoch = currentRxTime.dbl();
        nl[sourceID].T_ON = westMsg->getT_on() / 5;

/*
        // Get the information about the token and remaining timing
        tokenInfo = pkt->getToken();

        if ((tokenInfo & TOKEN_CODE) == TOKEN_CODE) {
            // Cancel a previously scheduled timer (if any)
            if (timeSlotAlignment->isScheduled())
                cancelEvent(timeSlotAlignment);
        }
*/

        if (westMsg->getDstAddr() == nodeID
                || westMsg->getDstAddr() == BROADCAST_ADDRESS) {
#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_RX)) {
                // Record this reception
                westLog.pktRX_RngFINAL.timestamp[(westLog.pktRX_RngFINAL.howMany++) % DIM_LOG] = currentRxTime.dbl();
            }
#endif

            getParentModule()->getParentModule()->bubble(
                    "RANGING FINAL RICEVUTO");

            cMessage *resp = new cMessage();
            resp->setName(CMD_RANGING_REPORT);
            handleTransmission(resp, sourceID);
        }

        // Jump to the end of the function
        goto goodEnd;
    }

    // ********************************************
    //           RANGING REPORT PACKET
    // ********************************************
    if (westMsg->getFunctionCode() == PKT_RNG_REPORT_MSG) {
        // Parse the encapsulated packet accordingly
        //RangingReport *pkt = check_and_cast<RangingReport *>(
        //           westMsg->getEncapsulatedPacket());

        nl[sourceID].epoch = currentRxTime.dbl();
        nl[sourceID].T_ON = westMsg->getT_on() / 5;

/*
        // Get the information about the token and remaining timing
        tokenInfo = pkt->getToken();

        if ((tokenInfo & TOKEN_CODE) == TOKEN_CODE) {
            // Cancel a previously scheduled timer (if any)
            if (timeSlotAlignment->isScheduled())
                cancelEvent(timeSlotAlignment);
        }
*/

        if (westMsg->getDstAddr() == nodeID
                || westMsg->getDstAddr() == BROADCAST_ADDRESS) {
#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_RX)) {
                // Record this reception
                westLog.pktRX_RngREPORT.timestamp[(westLog.pktRX_RngREPORT.howMany++) % DIM_LOG] = currentRxTime.dbl();
            }
#endif

            getParentModule()->getParentModule()->bubble(
                    "RANGING REPORT RICEVUTO");

            // Close the ranging procedure
            nl[sourceID].rangingDone = RNG_DONE;
            nl[sourceID].lastRanging = currentRxTime.dbl();

            // TODO: the computed value of the distance should be in the packet.
            // For testing and debugging mode, set the value of the distance to be traceable, as:
            nl[sourceID].distance[nodeID] = ((nodeID & 0x00FF) << 8)
                                    | (sourceID & 0x00FF);

            // Record closure of ranging process if it was still open with node sourceID ...
            if (rngDur[sourceID].isOpen) {
                rngDur[sourceID].isOpen = false;
#ifdef ENABLE_LOG
                if (LOG_CHECK_OPTION(logOption,LOG_MASK_RNGDUR)) {
                    // ... and compute duration
                    westLog.rngDuration.timestamp[(westLog.rngDuration.howMany++) % DIM_LOG] =
                            currentRxTime.dbl() - rngDur[sourceID].timestamp;
                }
#endif
                // Reset timestamp
                rngDur[sourceID].timestamp = 0;
            }
        }

        // Jump to the end of the function
        goto goodEnd;
    }

    // ********************************************
    //           NETWORK DATA PACKET
    // ********************************************
    if (westMsg->getFunctionCode() == PKT_NET_DATA) {
        // Parse the encapsulated packet accordingly
        NetData *pkt = check_and_cast<NetData *>(
                   westMsg->getEncapsulatedPacket());

        // Get the information about the token and remaining timing
        tokenInfo = pkt->getToken();

        if ((tokenInfo & TOKEN_CODE) == TOKEN_CODE) {
            // Cancel a previously scheduled timer (if any)
            if (timeSlotAlignment->isScheduled())
                cancelEvent(timeSlotAlignment);

            // The source node is carrying the token
            currentNode = sourceID;

            // Compute the timing for the next schedule
            double nextTimeout = (double) (tokenInfo & ~TOKEN_CODE) / 1000;
            scheduleAt(currentRxTime + nextTimeout - POLLduration.dbl() / 1000,
                    timeSlotAlignment);
        }

        nl[sourceID].epoch = currentRxTime.dbl();

        if (westMsg->getDstAddr() == nodeID
                || westMsg->getDstAddr() == BROADCAST_ADDRESS) {
#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_RX)) {
                // Record this reception
                westLog.pktRX_NET.timestamp[(westLog.pktRX_NET.howMany++) % DIM_LOG] = currentRxTime.dbl();
            }
#endif
            EV<< "Passing up contained packet `" << westMsg->getName() << "' to higher layer\n";

            // Send Up just the payload
            sendUp(westMsg->decapsulate());
        }

        // Jump to the end of the function
        goto goodEnd;
    }

    // In every other case, this error should be raised
    throw cRuntimeError("Unexpected packet code received from lower layer");

    goodEnd:
    delete frame;
    delete westMsg;
}

void WestMac::handleSelfMsg(cMessage* message) {

    if (message == dutyCycleReport) {
        // recordScalar("dutyCycle", dutyCycle);
        dutyCycle = 0;
        t_on = 0; // reset my t_on every second
        scheduleAt(simTime() + 1, message);

        return;
    }

    if (message == timeSlotAlignment) {
        // Time to update the currentNode
        currentNode++;
        if (currentNode == numNodes)
            currentNode = 0;

        if (currentNode == nodeID) {
            for (int i = 0; i < MAX_NODES; i++) {
                if ((simTime().dbl() - nl[i].epoch > 1) && i != nodeID) {
                    nl[i].isPresent = false;
                }
            }
            // My turn to get the token!
            handleTransmission(message, 0);
        }
        if (!timeSlotAlignment->isScheduled())
            scheduleAt(simTime() + timeSlot, timeSlotAlignment);

        return;
    }

#if 0
    if (message == macInertial) {
        cMessage* newMsg = new cMessage();
        newMsg->setName(CMD_INERTIAL);
        handleCommand(newMsg);

        return;
    }
#endif

    if (message == switchOff) {
        synchronized = false;
        nodeOff = true;

        return;
    }

    if (message == switchOn) {
        nodeOff = false;
        getParentModule()->getParentModule()->getDisplayString().setTagArg("i",
                0, "device/pocketpc_s");

        return;
    }

    throw cRuntimeError("model error: unknown self message");
}


uint16_t WestMac::computePayloadSize(uint8_t typeCode) {
    // Compute the dimension of the known packets
    uint16_t payloadSize = 0;

    switch (typeCode) {
    case PKT_KEEP_ALIVE:
        payloadSize += 1    // token
                    + 2     // epoch
                    + 1     // battery
                    + 1;    // temperature
        break;

    case PKT_HELLO:
        payloadSize += 1                    // token
                    + sizeof(FullPayload)   // FullPayload
                    + 2                     // pressure
                    + 2                     // accuracy
                    + sizeof(GPS);          // GPS
        break;

    case PKT_RNG_POLL:
        payloadSize += 1                    // token
                    + sizeof(FullPayload);  // FullPayload
        break;

    case PKT_RNG_RESPONSE:
        payloadSize += 1                    // activity
                    + 2                     // activityParam
                    + 1                     // token
                    + sizeof(FullPayload);  // FullPayload
        break;

    case PKT_RNG_FINAL_MSG:
        payloadSize += 5                  // pollTimestamp
                    + 5                   // responseTimestamp
                    + 5                   // finalTxTimestamp
                    + 1;                  // token
        break;

    case PKT_RNG_REPORT_MSG:
        payloadSize += 5                  // calculateToF
                    + 1;                  // token
        break;

    case PKT_NET_DATA:
        payloadSize += 1                    // token
                    + sizeof(FullPayload);  // FullPayload
        break;

    default:
        throw cRuntimeError("MODEL ERROR: unknown packet type to compute duration!");
    }

    return payloadSize;
}

simtime_t WestMac::computeDuration(int payloadSize) {
    // Compute the actual duration of the packet based on the interpolation formula:
    // <Duration> = a * <PayloadSize> + b

    const double a = 0.08;
    const double b = 3.06;
    simtime_t duration = a * (double)payloadSize + b;

    return duration / 1000; // [ms]
}


simtime_t WestMac::computeWestDuration(uint8_t typeCode) {
    // Compute the actual duration of the known packets

    // Base value for the payloadSize is the size of WESTFRAME
    uint16_t payloadSize = WEST_FRAME_SIZE + computePayloadSize(typeCode);

    simtime_t duration = computeDuration(payloadSize);

    return duration;
}


WestFrame *WestMac::buildWestFrame(uint8_t typeCode, uint16_t dst) {
    // Build the different packet types and return the pointer to it

    // Get time reference now. It might be used later for packet formatting
    simtime_t nowTime = simTime();

    // Update Packet Sequence Number
    packetCounter++;
    // Reset packetCounter if needed
    if (packetCounter == 255) {
        packetCounter = 1;
    }

    // ********************************************
    //              KEEP ALIVE PACKET
    // ********************************************
    if (typeCode == PKT_KEEP_ALIVE) {
        KeepAlive *msdu = new KeepAlive();
        WestFrame* tmpData = new WestFrame("FrameKA");

        // Update t_on value
        t_on = t_on + KEEPduration.dbl();

        /* construct MPDU */
        tmpData->setName("FrameKA");
        tmpData->setFrmCtrl(frmCtrl);
        tmpData->setBdsn(packetCounter);
        tmpData->setDstPanId(PANcoordinator);
        tmpData->setDstAddr(dst);
        tmpData->setSrcAddr(nodeID);
        tmpData->setT_on((uint8_t) ceil(t_on * 5)); // t_on expressed as 1/5 of ms
        // Add function code (i.e., type) to the packet information
        tmpData->setFunctionCode(PKT_KEEP_ALIVE);

        msdu->setName("KAPkt");

        uint8_t t = (uint8_t) ceil(timeSlot.dbl() * 1000);    // [ms]
        msdu->setToken(TOKEN_CODE | t); // Get the Token and set the remaining duration of the timeslot
        msdu->setEpoch(pktEpoch++);

        msdu->setByteLength(computePayloadSize(PKT_KEEP_ALIVE));

        tmpData->encapsulate(msdu);     // the length of msdu is added to mpdu

        // set length and encapsulate msdu
        tmpData->setByteLength(WEST_FRAME_SIZE + computePayloadSize(PKT_KEEP_ALIVE));
        tmpData->setDuration(computeWestDuration(PKT_KEEP_ALIVE));

        EV << "[MAC]: MPDU constructed: " << tmpData->getName() << ", #" << (int)tmpData->getBdsn() << ", " << tmpData->getByteLength() << " Bytes" << endl;
        return tmpData;
    }

    // ********************************************
    //              HELLO PACKET
    // ********************************************
    if (typeCode == PKT_HELLO) {
        HelloPkt *msdu = new HelloPkt();
        WestFrame* tmpData = new WestFrame("FrameHELLO");
        FullPayload pay;

        // Update t_on value
        t_on = t_on + HELLOduration.dbl();

        /* construct MPDU */
        tmpData->setName("FrameHello");
        tmpData->setFrmCtrl(frmCtrl);
        tmpData->setBdsn(packetCounter);
        tmpData->setDstPanId(PANcoordinator);
        tmpData->setDstAddr(dst);
        tmpData->setSrcAddr(nodeID);
        tmpData->setT_on((uint8_t) ceil(t_on * 5)); // t_on expressed as 1/5 of ms
        // Add function code (i.e., type) to the packet information
        tmpData->setFunctionCode(PKT_HELLO);

        msdu->setName("HelloPkt");

        uint8_t t = (uint8_t) ceil(timeSlot.dbl() * 1000);    // [ms]
        msdu->setToken(TOKEN_CODE | t); // Get the Token and set the remaining duration of the timeslot

        pay.epoch = pktEpoch; // TODO: update pktEpoch for all the nodes based on some policy

        // Build rangingDone information
        uint16_t rDoneTemp = 0;
        uint8_t checkNumber = 0;

        // Scan all neighbors and check with which ranging has been done
        for (int i = 0; i < MAX_NODES; i++){
            if ((nl[i].rangingDone == RNG_DONE)
                    && (nowTime - nl[i].lastRanging) < timeGenerationInterval) {
                rDoneTemp |= (1 << i);
                checkNumber++;
            }
        }

        // Safety model check
        if (checkNumber > maxNeighborsPortable)
            throw cRuntimeError("MODEL ERROR: Too many valid distance with neighbors");

        pay.rangingDone = rDoneTemp;

        if (rDoneTemp != 0){
            // Get the distances computed from each of the available valid nodes
            uint8_t j = 0;
            uint8_t i = 0;
            while (rDoneTemp != 0) {
                uint16_t MASK = (1 << i);

                if (rDoneTemp & MASK){
                    pay.distance[j++] = nl[i].distance[nodeID];
                    rDoneTemp &= (~MASK);   // Clear that bit
                }
                i++;
            }
        } else
            memset(pay.distance, 0, maxNeighborsPortable*sizeof(uint16_t));


        // Record the information into the packet's FULL Payload
        msdu->setFullPayload(pay);
        msdu->setByteLength(computePayloadSize(PKT_HELLO));

        tmpData->encapsulate(msdu);   // the length of msdu is added to mpdu

        // set length and encapsulate msdu
        tmpData->setByteLength(WEST_FRAME_SIZE + computePayloadSize(PKT_HELLO));
        tmpData->setDuration(computeWestDuration(PKT_HELLO));

        EV << "[MAC]: MPDU constructed: " << tmpData->getName() << ", #" << (int)tmpData->getBdsn() << ", " << tmpData->getByteLength() << " Bytes" << endl;
        return tmpData;
    }

    // ********************************************
    //              RANGING POLL PACKET
    // ********************************************
    if (typeCode == PKT_RNG_POLL) {
        //WestFrame* tmpData = &wstFrame;
        WestFrame* tmpData = new WestFrame("FrameRNGPoll");
        FullPayload pay;

        // Update t_on value
        t_on = t_on + POLLduration.dbl();

        /* construct MPDU */
        tmpData->setName("FrameRNGPoll");
        tmpData->setFrmCtrl(frmCtrl);
        tmpData->setBdsn(packetCounter);
        tmpData->setDstPanId(PANcoordinator);
        tmpData->setDstAddr(dst);
        tmpData->setSrcAddr(nodeID);
        tmpData->setT_on((uint8_t) ceil(t_on * 5)); // t_on expressed as 1/5 of ms
        // Add function code (i.e., type) to the packet information
        tmpData->setFunctionCode(PKT_RNG_POLL);

        uint8_t t = (uint8_t) ceil(timeSlot.dbl() * 1000);    // [ms]

        RangingPoll *msdu = new RangingPoll();

        msdu->setToken(TOKEN_CODE | t); // Get the Token and set the remaining duration of the timeslot

        msdu->setName("RNGPollPkt");

        pay.epoch = pktEpoch; // TODO: update pktEpoch for all the nodes based on some policy

        // Build rangingDone information
        uint16_t rDoneTemp = 0;
        uint8_t checkNumber = 0;

        // Scan all neighbors and check with which ranging has been done
        for (int i = 0; i < MAX_NODES; i++){
            if ((nl[i].rangingDone == RNG_DONE)
                    && (nowTime - nl[i].lastRanging) < timeGenerationInterval) {
                rDoneTemp |= (1 << i);
                checkNumber++;
            }
        }

        // Safety model check
        if (checkNumber > maxNeighborsPortable)
            throw cRuntimeError("MODEL ERROR: Too many valid distance with neighbors");

        pay.rangingDone = rDoneTemp;

        if (rDoneTemp != 0){
            // Get the distances computed from each of the available valid nodes
            uint8_t j = 0;
            uint8_t i = 0;
            while (rDoneTemp != 0) {
                uint16_t MASK = (1 << i);

                if (rDoneTemp & MASK){
                    pay.distance[j++] = nl[i].distance[nodeID];
                    rDoneTemp &= (~MASK);   // Clear that bit
                }
                i++;
            }
        } else
            memset(pay.distance, 0, maxNeighborsPortable*sizeof(uint16_t));


        // Record the information into the packet's FULL Payload
        msdu->setFullPayload(pay);
        msdu->setByteLength(computePayloadSize(PKT_RNG_POLL));

        tmpData->encapsulate(msdu);   // the length of msdu is added to mpdu

        // set length and encapsulate msdu
        tmpData->setByteLength(WEST_FRAME_SIZE + computePayloadSize(PKT_RNG_POLL));
        tmpData->setDuration(computeWestDuration(PKT_RNG_POLL));

        EV << "[MAC]: MPDU constructed: " << tmpData->getName() << ", #" << (int)tmpData->getBdsn() << ", " << tmpData->getByteLength() << " Bytes" << endl;
        return tmpData;
    }

    // ********************************************
    //           RANGING RESPONSE PACKET
    // ********************************************
    if (typeCode == PKT_RNG_RESPONSE) {
        WestFrame* tmpData = new WestFrame("FrameRNGResponse");
        FullPayload pay;

        // Update t_on value
        t_on = t_on + RESPONSEduration.dbl();

        /* construct MPDU */
        tmpData->setName("FrameRNGResponse");
        tmpData->setFrmCtrl(frmCtrl);
        tmpData->setBdsn(packetCounter);
        tmpData->setDstPanId(PANcoordinator);
        tmpData->setDstAddr(dst);
        tmpData->setSrcAddr(nodeID);
        tmpData->setT_on((uint8_t) ceil(t_on * 5)); // t_on expressed as 1/5 of ms
        // Add function code (i.e., type) to the packet information
        tmpData->setFunctionCode(PKT_RNG_RESPONSE);

        uint8_t t = (uint8_t) ceil(
                timeSlot.dbl() * 1000 - POLLduration.dbl()); // [ms]

        RangingResponse *msdu = new RangingResponse();

        msdu->setToken(0x00 | t); // Leave the Token to the owner and set the remaining duration of the timeslot

        msdu->setName("RNGResponsePkt");

        pay.epoch = pktEpoch; // TODO: update pktEpoch for all the nodes based on some policy

        // Build rangingDone information
        uint16_t rDoneTemp = 0;
        uint8_t checkNumber = 0;

        // Scan all neighbors and check with which ranging has been done
        for (int i = 0; i < MAX_NODES; i++){
            if ((nl[i].rangingDone == RNG_DONE)
                    && (nowTime - nl[i].lastRanging) < timeGenerationInterval) {
                rDoneTemp |= (1 << i);
                checkNumber++;
            }
        }

        // Safety model check
        if (checkNumber > maxNeighborsPortable)
            throw cRuntimeError("MODEL ERROR: Too many valid distance with neighbors");

        pay.rangingDone = rDoneTemp;

        if (rDoneTemp != 0){
            // Get the distances computed from each of the available valid nodes
            uint8_t j = 0;
            uint8_t i = 0;
            while (rDoneTemp != 0) {
                uint16_t MASK = (1 << i);

                if (rDoneTemp & MASK){
                    pay.distance[j++] = nl[i].distance[nodeID];
                    rDoneTemp &= (~MASK);   // Clear that bit
                }
                i++;
            }
        } else
            memset(pay.distance, 0, maxNeighborsPortable*sizeof(uint16_t));


        // Record the information into the packet's FULL Payload
        msdu->setFullPayload(pay);
        msdu->setByteLength(computePayloadSize(PKT_RNG_RESPONSE));

        tmpData->encapsulate(msdu);   // the length of msdu is added to mpdu

        // set length and encapsulate msdu
        tmpData->setByteLength(WEST_FRAME_SIZE + computePayloadSize(PKT_RNG_RESPONSE));
        tmpData->setDuration(computeWestDuration(PKT_RNG_RESPONSE));

        EV << "[MAC]: MPDU constructed: " << tmpData->getName() << ", #" << (int)tmpData->getBdsn() << ", " << tmpData->getByteLength() << " Bytes" << endl;
        return tmpData;
    }

    // ********************************************
    //           RANGING FINAL PACKET
    // ********************************************
    if (typeCode == PKT_RNG_FINAL_MSG) {
        WestFrame* tmpData = new WestFrame("FrameRNGFinal");

        // Update t_on value
        t_on = t_on + FINALduration.dbl();

        /* construct MPDU */
        tmpData->setName("FrameRNGFinal");
        tmpData->setFrmCtrl(frmCtrl);
        tmpData->setBdsn(packetCounter);
        tmpData->setDstPanId(PANcoordinator);
        tmpData->setDstAddr(dst);
        tmpData->setSrcAddr(nodeID);
        tmpData->setT_on((uint8_t) ceil(t_on * 5)); // t_on expressed as 1/5 of ms
        // Add function code (i.e., type) to the packet information
        tmpData->setFunctionCode(PKT_RNG_FINAL_MSG);

        uint8_t t = (uint8_t) ceil(
                timeSlot.dbl() * 1000 - POLLduration.dbl()
                - RESPONSEduration.dbl());    // [ms]

        RangingFinal *msdu = new RangingFinal();

        msdu->setToken(TOKEN_CODE | t); // Get the Token and set the remaining duration of the timeslot

        msdu->setName("RNGFinalPkt");
        msdu->setByteLength(computePayloadSize(PKT_RNG_FINAL_MSG));

        tmpData->encapsulate(msdu);   // the length of msdu is added to mpdu

        // set length and encapsulate msdu
        tmpData->setByteLength(WEST_FRAME_SIZE + computePayloadSize(PKT_RNG_FINAL_MSG));
        tmpData->setDuration(computeWestDuration(PKT_RNG_FINAL_MSG));

        EV << "[MAC]: MPDU constructed: " << tmpData->getName() << ", #" << (int)tmpData->getBdsn() << ", " << tmpData->getByteLength() << " Bytes" << endl;
        return tmpData;
    }

    // ********************************************
    //           RANGING REPORT PACKET
    // ********************************************
    if (typeCode == PKT_RNG_REPORT_MSG) {
        WestFrame* tmpData = new WestFrame("FrameRNGReport");

        // Update t_on value
        t_on = t_on + REPORTduration.dbl();

        /* construct MPDU */
        tmpData->setName("FrameRNGReport");
        tmpData->setFrmCtrl(frmCtrl);
        tmpData->setBdsn(packetCounter);
        tmpData->setDstPanId(PANcoordinator);
        tmpData->setDstAddr(dst);
        tmpData->setSrcAddr(nodeID);
        tmpData->setT_on((uint8_t) ceil(t_on * 5)); // t_on expressed as 1/5 of ms
        // Add function code (i.e., type) to the packet information
        tmpData->setFunctionCode(PKT_RNG_REPORT_MSG);

        uint8_t t = (uint8_t) ceil(
                timeSlot.dbl() * 1000 - POLLduration.dbl()
                - RESPONSEduration.dbl() - FINALduration.dbl()); // [ms]

        RangingReport *msdu = new RangingReport();

        msdu->setToken(0x00 | t); // Leave the Token to the owner and set the remaining duration of the timeslot

        msdu->setName("RNGReportPkt");
        msdu->setByteLength(computePayloadSize(PKT_RNG_REPORT_MSG));

        tmpData->encapsulate(msdu);   // the length of msdu is added to mpdu

        // set length and encapsulate msdu
        tmpData->setByteLength(WEST_FRAME_SIZE + computePayloadSize(PKT_RNG_REPORT_MSG));
        tmpData->setDuration(computeWestDuration(PKT_RNG_REPORT_MSG));

        EV << "[MAC]: MPDU constructed: " << tmpData->getName() << ", #" << (int)tmpData->getBdsn() << ", " << tmpData->getByteLength() << " Bytes" << endl;
        return tmpData;
    }

    // ********************************************
    //           NETWORK DATA PACKET
    // ********************************************
    if (typeCode == PKT_NET_DATA) {
        WestFrame* tmpData = new WestFrame("FrameData");
        NetData *msdu = check_and_cast<NetData*>(
                transmissionQueue[queueIt - 1]);

        // Update t_on value
        t_on = t_on + DATAduration.dbl();

        /* construct MPDU */
        tmpData->setName("FrameData");
        tmpData->setFrmCtrl(frmCtrl);
        tmpData->setBdsn(packetCounter);
        tmpData->setDstPanId(PANcoordinator);
        tmpData->setDstAddr(dst);
        tmpData->setSrcAddr(nodeID);
        tmpData->setT_on((uint8_t) ceil(t_on * 5)); // t_on expressed as 1/5 of ms
        // Add function code (i.e., type) to the packet information
        tmpData->setFunctionCode(PKT_NET_DATA);

        // TODO: update the remaining timer based on how many NET DATA
        // packets have been sent so far in this slot
        uint8_t t = (uint8_t) ceil(timeSlot.dbl() * 1000);    // [ms]

        msdu->setToken(TOKEN_CODE | t); // Get the Token for the duration of the timeslot

        msdu->setName("DataPkt");
        msdu->setByteLength(computePayloadSize(PKT_NET_DATA));

        tmpData->encapsulate(msdu);   // the length of msdu is added to mpdu

        // set length and encapsulate msdu
        tmpData->setByteLength(WEST_FRAME_SIZE + computePayloadSize(PKT_NET_DATA));
        tmpData->setDuration(computeWestDuration(PKT_NET_DATA));

        EV << "[MAC]: MPDU constructed: " << tmpData->getName() << ", #" << (int)tmpData->getBdsn() << ", " << tmpData->getByteLength() << " Bytes" << endl;
        return tmpData;
    }

    throw cRuntimeError("MODEL ERROR: Unknown packet code to build!!");

    return NULL;
}


uint8_t WestMac::handleTransmission(cMessage *msg, uint16_t srcId) {
    // Record the current time now for any reference in this function
    simtime_t currentTxTime = simTime();

    // Controllo quanto tempo impiega un nodo a fare ranging con i suoi vicini
    /*************************************************************/
//    int c = 0;
//
//    for (int i = 0; i < numNodes; i++) {
//        if (nl[i].rangingDone == RNG_DONE) {
//            c++;
//        }
//        if (c == 3 && i == numNodes-1 && countNeighbors == 5) {
//            double tempoTre = currentTxTime.dbl();
//            compNL_three++;
//            countNeighbors = 3;
//            tempoTre_array[compNL_three] = tempoTre;
//        }
//        if (c == 4 && i == numNodes-1 && countNeighbors == 3) {
//            double tempoQuattro = currentTxTime.dbl();
//            compNL_four++;
//            countNeighbors = 4;
//            tempoQuattro_array[compNL_four] = tempoQuattro;
//        }
//        if (c == 5 && i == numNodes-1 && countNeighbors == 4) {
//            double tempoCinque = currentTxTime.dbl();
//            compNL_five++;
//            countNeighbors = 5;
//            tempoCinque_array[compNL_five] = tempoCinque;
//
//            // Reset of rangingDone
//            for (int j = 1; j < numNodes + 1; j++) {
//                if (nl[j].rangingDone != RNG_UNSET){
//                    // Reset Ranging Done
//                    nl[j].rangingDone = RNG_UNSET;
//
//                    if (rngDur[j].isOpen){
//                        // Reset ranging open and record unfinished ranging processes
//                        rngDur[j].isOpen = false;
//#ifdef ENABLE_LOG
//                        if (LOG_CHECK_OPTION(logOption,LOG_MASK_RNGDUR)) {
//                            westLog.rngUnfinished.timestamp[(westLog.rngUnfinished.howMany++) % DIM_LOG] = currentTxTime.dbl();
//                        }
//#endif
//                        // Reset timestamp
//                        rngDur[j].timestamp = 0;
//                    }
//                }
//            }
//        }
//    }


    /*************************************************************/

    // ********************************************
    //              START NETWORK
    // ********************************************
    if (!strcmp(msg->getName(), CMD_START)) {

        rangingToBeDone = true;

        WestFrame* tmpData = buildWestFrame(PKT_KEEP_ALIVE, BROADCAST_ADDRESS);

        // Send around a duplicate of the packet to avoid issues with ownership in the simulation environment
        startTransmitting(tmpData->dup());

        // Record me as transmitter node for the token
        currentNode = nodeID;
        scheduleAt(simTime() + timeSlot, timeSlotAlignment);

#ifdef ENABLE_LOG
        if (LOG_CHECK_OPTION(logOption,LOG_MASK_TX)) {
            // Record this transmission
            westLog.pktTX_KA.timestamp[(westLog.pktTX_KA.howMany++) % DIM_LOG] = currentTxTime.dbl();
        }
#endif

        delete tmpData;

        return TX_SUCCESS;
    }

    // ********************************************
    //        CHECK IF SMTHG TO TRANSMIT
    // ********************************************
    if (queueIt > 0) {

        // Check if there is enough resource for transmission
        if ((transmissionLimit - t_on >= DATAduration.dbl())) {
            WestFrame* tmpData = buildWestFrame(PKT_NET_DATA, BROADCAST_ADDRESS);

            // Send around a duplicate of the packet to avoid issues with
            // ownership in the simulation environment
            startTransmitting(tmpData->dup());

            queueIt--;

            // TODO: check the possibility to implement another sending
            // of data before leaving the slot, if queue is not empty

#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_TX)) {
                // Record this transmission
                westLog.pktTX_NET.timestamp[(westLog.pktTX_NET.howMany++) % DIM_LOG] = currentTxTime.dbl();
            }
#endif

            delete tmpData;

            return TX_SUCCESS;
        }

        // Not enough resource: try sending Hello or KA
        if ((transmissionLimit - t_on >= HELLOduration.dbl())) {
            WestFrame* tmpData = buildWestFrame(PKT_HELLO, BROADCAST_ADDRESS);

            // Send around a duplicate of the packet to avoid issues with ownership
            // in the simulation environment
            startTransmitting(tmpData->dup());

#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_NODC)) {
                // Record this event
                westLog.pktTX_HELLO.timestamp[(westLog.pktTX_HELLO.howMany++) % DIM_LOG] = currentTxTime.dbl();
            }
#endif

            delete tmpData;
            return TX_NO_DATA;
        }

        if ((transmissionLimit - t_on >= KEEPduration.dbl())) {
            WestFrame* tmpData = buildWestFrame(PKT_KEEP_ALIVE, BROADCAST_ADDRESS);

            // Send around a duplicate of the packet to avoid issues with ownership in the simulation environment
            startTransmitting(tmpData->dup());

#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_NODC)) {
                // Record this event
                westLog.pktTX_KA.timestamp[(westLog.pktTX_KA.howMany++) % DIM_LOG] = currentTxTime.dbl();
            }
#endif

            delete tmpData;
            return TX_NO_DATA;
        }

        // Else: there is no resource left for sending anything.
        // Track this situation with counters: NO TIME LEFT FOR HELLO NOR KA!!
#ifdef ENABLE_LOG
        if (LOG_CHECK_OPTION(logOption,LOG_MASK_NODC)) {
            // Track this situation with counters
            westLog.noPktTX.timestamp[(westLog.noPktTX.howMany++) % DIM_LOG] = currentTxTime.dbl();
        }
#endif

        return TX_T_ON;
    }

    // ********************************************
    //          NEED TO SEND RANGING RESPONSE
    // ********************************************
    if (!strcmp(msg->getName(), CMD_RANGING_RESPONSE)) {

        // The node queried checks if enough resource is available to
        // accomplish a full ranging process
        if (transmissionLimit - t_on > responderLimit) {
            WestFrame* tmpData = buildWestFrame(PKT_RNG_RESPONSE, srcId);

            // Send around a duplicate of the packet to avoid issues with ownership in the simulation environment
            startTransmitting(tmpData->dup());

            if (!DSTWR) {
                // In SINGLE SIDED TWO-WAY RANGING mode, at this moment the other node knows
                // the value of the distance

                // Close the ranging procedure - in this case with pending,
                // waiting for overhearing the distance value computed by the other node
                nl[srcId].rangingDone = RNG_PENDING;
                nl[srcId].lastRanging = currentTxTime.dbl();
            }

#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_TX)) {
                // Record this transmission
                westLog.pktTX_RngRESPONSE.timestamp[(westLog.pktTX_RngRESPONSE.howMany++) % DIM_LOG] = currentTxTime.dbl();
            }
#endif

            delete tmpData;

            return TX_SUCCESS;
        }

        // Else: Not enough resource left to reply to POLL
        // Simply skip to respond

#ifdef ENABLE_LOG
        if (LOG_CHECK_OPTION(logOption,LOG_MASK_RNGERR)) {
            // Track this situation with counters
            westLog.noRngRESPONSE.timestamp[(westLog.noRngRESPONSE.howMany++) % DIM_LOG] = currentTxTime.dbl();
        }
#endif

        return TX_T_ON;
    }

    // ********************************************
    //          NEED TO SEND RANGING FINAL
    // ********************************************
    if (!strcmp(msg->getName(), CMD_RANGING_FINAL)) {

        if (transmissionLimit - t_on > FINALduration.dbl()) {
            WestFrame* tmpData = buildWestFrame(PKT_RNG_FINAL_MSG, srcId);

            // Send around a duplicate of the packet to avoid issues with ownership
            // in the simulation environment
            startTransmitting(tmpData->dup());

#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_TX)) {
                // Record this transmission
                westLog.pktTX_RngFINAL.timestamp[(westLog.pktTX_RngFINAL.howMany++) % DIM_LOG] = currentTxTime.dbl();
            }
#endif

            delete tmpData;
            return TX_SUCCESS;
        }

        // Else: Not enough resource left to send Final. This should never happen since the full ranging time
        // is taken into account at the beginning of the the procedure

        throw cRuntimeError("MODEL ERROR: NO TIME LEFT FOR RANGING FINAL!!");

        return TX_T_ON;
    }

    // ********************************************
    //          NEED TO SEND RANGING REPORT
    // ********************************************
    if (!strcmp(msg->getName(), CMD_RANGING_REPORT)) {

        if (transmissionLimit - t_on > REPORTduration.dbl()) {
            WestFrame* tmpData = buildWestFrame(PKT_RNG_REPORT_MSG, srcId);

            // Send around a duplicate of the packet to avoid issues with ownership
            // in the simulation environment
            startTransmitting(tmpData->dup());

            // Close the ranging procedure
            nl[srcId].rangingDone = RNG_DONE;
            nl[srcId].lastRanging = simTime().dbl();

            // For testing and debugging mode, set the value of the distance to be traceable, as:
            nl[srcId].distance[nodeID] = ((nodeID & 0x00FF) << 8) | (srcId & 0x00FF);

#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_TX)) {
                // Record this transmission
                westLog.pktTX_RngREPORT.timestamp[(westLog.pktTX_RngREPORT.howMany++) % DIM_LOG] = currentTxTime.dbl();
            }
#endif

            // Record closure of ranging process with node sourceID ...
            rngDur[srcId].isOpen = false;
#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_RNGDUR)) {
                // ... and duration
                westLog.rngDuration.timestamp[(westLog.rngDuration.howMany++) % DIM_LOG] =
                        currentTxTime.dbl() - rngDur[srcId].timestamp;
            }
#endif
            // Reset timestamp
            rngDur[srcId].timestamp = 0;

            delete tmpData;
            return TX_SUCCESS;
        }

        // Else: Not enough resource left to send Report. This should never happen since
        // the full ranging time is taken into account at the beginning of the the procedure

        throw cRuntimeError("MODEL ERROR: NO TIME LEFT FOR RANGING REPORT!!");

        return TX_T_ON;
    }

    // ********************************************
    //              START RANGING (POLL)
    // ********************************************
    if (packetCounter > 0 && rangingToBeDone) {

        // Ranging process begins only if node has enough resource to accomplish it to its end
        if (transmissionLimit - t_on > rangerLimit) {

            // Search a neighbor to start ranging with
            uint16_t r = 1 + random() % numNodes;
            bool performRanging = false;
            bool responderFound = false;
            int count = 0;

            do {
                if (
                        (r != nodeID)           // Don't check the node itself
                        && (nl[r].isPresent == true)  // Check only present neighbors
                        && (nl[r].rangingDone) == RNG_UNSET     // Check nodes with which ranging has not been done
                        && (currentTxTime.dbl() - nl[r].lastRanging > 0.3)    // Check nodes with which ranging value is old
                        && (nl[r].T_ON + responderLimit < transmissionLimit)     // Check if neighbor is able to accomplish the ranging process
                    )
                {
                    performRanging = true;
                    responderFound = true;
                    break;
                } else {
                    r++;
                    if (r == numNodes)
                        r = 0;
                    count++;
                }
                if (count == numNodes) {
                    responderFound = false;
                    performRanging = true;
                }
            } while (performRanging == false);

            if (responderFound) {
                // Found some neighbor to start ranging with
                WestFrame* tmpData = buildWestFrame(PKT_RNG_POLL, r);

                // Send around a duplicate of the packet to avoid issues with ownership
                // in the simulation environment
                startTransmitting(tmpData->dup());

#ifdef ENABLE_LOG
                if (LOG_CHECK_OPTION(logOption,LOG_MASK_TX)) {
                    // Record this transmission
                    westLog.pktTX_RngPOLL.timestamp[(westLog.pktTX_RngPOLL.howMany++) % DIM_LOG] = currentTxTime.dbl();
                }
#endif

                // Record open of ranging process with node r
                rngDur[r].isOpen = true;
                rngDur[r].timestamp = currentTxTime.dbl();

                delete tmpData;
                return TX_SUCCESS;
            }

            // Couldn't find a neighbor to start ranging with
            // Check if node can send HELLO or KEEP ALIVE packet, based on remaining T_ON

            if ((transmissionLimit - t_on >= HELLOduration.dbl())) {
                WestFrame* tmpData = buildWestFrame(PKT_HELLO, BROADCAST_ADDRESS);

                // Send around a duplicate of the packet to avoid issues with ownership
                // in the simulation environment
                startTransmitting(tmpData->dup());

#ifdef ENABLE_LOG
                if (LOG_CHECK_OPTION(logOption,LOG_MASK_RNGNOTFOUND)) {
                    // Record this event
                    westLog.pktTX_RngHELLO_NONEIGH.timestamp[(westLog.pktTX_RngHELLO_NONEIGH.howMany++) % DIM_LOG] = currentTxTime.dbl();
                }
#endif

                delete tmpData;
                return TX_NO_RNG;
            }

            if ((transmissionLimit - t_on >= KEEPduration.dbl())) {
                WestFrame* tmpData = buildWestFrame(PKT_KEEP_ALIVE, BROADCAST_ADDRESS);

                // Send around a duplicate of the packet to avoid issues with ownership
                // in the simulation environment
                startTransmitting(tmpData->dup());

#ifdef ENABLE_LOG
                if (LOG_CHECK_OPTION(logOption,LOG_MASK_RNGNOTFOUND)) {
                    // Record this event
                    westLog.pktTX_RngKA_NONEIGH.timestamp[(westLog.pktTX_RngKA_NONEIGH.howMany++) % DIM_LOG] = currentTxTime.dbl();
                }
#endif

                delete tmpData;
                return TX_NO_RNG;
            }

#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_NODC)) {
                // Track this situation with counters
                westLog.noPktTX.timestamp[(westLog.noPktTX.howMany++) % DIM_LOG] = currentTxTime.dbl();
            }
#endif

            return TX_T_ON;
        }

        // Node couldn't start ranging.
        // Check if node can send HELLO or KEEP ALIVE packet, based on remaining T_ON

        if ((transmissionLimit - t_on >= HELLOduration.dbl())) {
            WestFrame* tmpData = buildWestFrame(PKT_HELLO, BROADCAST_ADDRESS);

            // Send around a duplicate of the packet to avoid issues with ownership
            // in the simulation environment
            startTransmitting(tmpData->dup());

#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_NODC)) {
                // Record this transmission
                westLog.pktTX_RngHELLO.timestamp[(westLog.pktTX_RngHELLO.howMany++) % DIM_LOG] = currentTxTime.dbl();
            }
#endif

            delete tmpData;
            return TX_NO_RNG;
        }

        if ((transmissionLimit - t_on >= KEEPduration.dbl())) {
            WestFrame* tmpData = buildWestFrame(PKT_KEEP_ALIVE, BROADCAST_ADDRESS);

            // Send around a duplicate of the packet to avoid issues with ownership
            // in the simulation environment
            startTransmitting(tmpData->dup());

#ifdef ENABLE_LOG
            if (LOG_CHECK_OPTION(logOption,LOG_MASK_NODC)) {
                // Record this transmission
                westLog.pktTX_RngKA.timestamp[(westLog.pktTX_RngKA.howMany++) % DIM_LOG] = currentTxTime.dbl();
            }
#endif

            delete tmpData;
            return TX_NO_RNG;
        }

#ifdef ENABLE_LOG
        if (LOG_CHECK_OPTION(logOption,LOG_MASK_NODC)) {
            // Track this situation with counters
            westLog.noPktTX.timestamp[(westLog.noPktTX.howMany++) % DIM_LOG] = currentTxTime.dbl();
        }
#endif

        return TX_T_ON;
    }

    // ********************************************
    //              NOTHING TO DO
    // ********************************************

    // Check if enough resource is available to send a HELLO or Keep Alice packet
    // based on the remaining T_ON
    // With the condition packetCounter != 0 nodes are forced to send a first round
    // of Keep Alive packet
    if ((transmissionLimit - t_on >= HELLOduration.dbl())
            && packetCounter != 0) {
        WestFrame* tmpData = buildWestFrame(PKT_HELLO, BROADCAST_ADDRESS);

        // Send around a duplicate of the packet to avoid issues with ownership in the simulation environment
        startTransmitting(tmpData->dup());

#ifdef ENABLE_LOG
        if (LOG_CHECK_OPTION(logOption,LOG_MASK_TX)) {
            // Record this transmission
            westLog.pktTX_HELLO.timestamp[(westLog.pktTX_HELLO.howMany++) % DIM_LOG] = currentTxTime.dbl();
        }
#endif

        delete tmpData;
        return TX_SUCCESS;
    }

    if ((transmissionLimit - t_on >= KEEPduration.dbl())) {
        rangingToBeDone = true;
        WestFrame* tmpData = buildWestFrame(PKT_KEEP_ALIVE, BROADCAST_ADDRESS);

        // Send around a duplicate of the packet to avoid issues with ownership in the simulation environment
        startTransmitting(tmpData->dup());

#ifdef ENABLE_LOG
        if (LOG_CHECK_OPTION(logOption,LOG_MASK_TX)) {
            // Record this transmission
            westLog.pktTX_KA.timestamp[(westLog.pktTX_KA.howMany++) % DIM_LOG] = currentTxTime.dbl();
        }
#endif

        delete tmpData;
        return TX_SUCCESS;
    }

#ifdef ENABLE_LOG
    if (LOG_CHECK_OPTION(logOption,LOG_MASK_NODC)) {
        // Track this situation with counters
        westLog.noPktTX.timestamp[(westLog.noPktTX.howMany++) % DIM_LOG] = currentTxTime.dbl();
    }
#endif

    return TX_T_ON;
}


void WestMac::acked(IdealWirelessFrame *frame) {
    Enter_Method_Silent
    ();

    if (lastSentPk
            && lastSentPk->getTreeId()
            == frame->getEncapsulatedPacket()->getTreeId()) {
        cancelEvent(ackTimeoutMsg);
        delete lastSentPk;
        lastSentPk = NULL;
        getNextMsgFromHL();
    }
}

IdealWirelessFrame *WestMac::encapsulate(cPacket *msg) {
    //    Ieee802Ctrl *ctrl = check_and_cast<Ieee802Ctrl*>(msg->removeControlInfo());
    IdealWirelessFrame *frame = new IdealWirelessFrame(msg->getName());
    //    frame->setByteLength(headerLength);
    //    frame->setSrc(ctrl->getSrc());
    //    frame->setDest(ctrl->getDest());
    //    frame->setSrcModuleId(lastSentPk ? getId() : -1);
    frame->encapsulate(msg);
    //    delete ctrl;
    return frame;
}


cPacket *WestMac::decapsulate(IdealWirelessFrame *frame) {
    // decapsulate and attach control info
    cPacket *packet = frame->decapsulate();
    //    Ieee802Ctrl *etherctrl = new Ieee802Ctrl();
    //    etherctrl->setSrc(frame->getSrc());
    //    etherctrl->setDest(frame->getDest());
    //    packet->setControlInfo(etherctrl);

    delete frame;
    return packet;
}

void WestMac::receiveChangeNotification(int category, const cObject *details) {
    Enter_Method_Silent
    ();
    MACBase::receiveChangeNotification(category, details);

    printNotificationBanner(category, details);

    if (category == NF_SYNCHRONIZED) {
        synchronized = true;
    }
}

void WestMac::finish() {

    file = fopen(fileName, "a+");
    if (file == NULL)
        throw cRuntimeError("Error trying to open the output file!");

/*
    char myNodeIdStrTre[5];
    sprintf(myNodeIdStrTre, "%i", nodeID);
    fputs(myNodeIdStrTre, file);
    fputs(";", file);

    char numeroViciniT[5];
    sprintf(numeroViciniT, "%i", numeroViciniTre);
    fputs(numeroViciniT, file);
    fputs(";", file);

    char numeroTotaleTre[5];
    sprintf(numeroTotaleTre, "%i", compNL_three);
    fputs(numeroTotaleTre, file);
    fputs(";", file);

    for (int i = 1; i < compNL_three + 1; i++) {
        char TempoTre[5];
        sprintf(TempoTre, "%.3f", tempoTre_array[i]);
        fputs(TempoTre, file);
        fputs(";", file);
        if (i == compNL_three)
            fputs("\n", file);
    }

    char myNodeIdStrQuattro[5];
    sprintf(myNodeIdStrQuattro, "%i", nodeID);
    fputs(myNodeIdStrQuattro, file);
    fputs(";", file);

    char numeroViciniQ[5];
    sprintf(numeroViciniQ, "%i", numeroViciniQuattro);
    fputs(numeroViciniQ, file);
    fputs(";", file);

    char numeroTotaleQuattro[5];
    sprintf(numeroTotaleQuattro, "%i", compNL_four);
    fputs(numeroTotaleQuattro, file);
    fputs(";", file);

    for (int i = 1; i < compNL_four + 1; i++) {
        char TempoQuattro[5];
        sprintf(TempoQuattro, "%.3f", tempoQuattro_array[i]);
        fputs(TempoQuattro, file);
        fputs(";", file);
        if (i == compNL_four)
            fputs("\n", file);
    }

    char myNodeIdStrCinque[5];
    sprintf(myNodeIdStrCinque, "%i", nodeID);
    fputs(myNodeIdStrCinque, file);
    fputs(";", file);

    char numeroViciniC[5];
    sprintf(numeroViciniC, "%i", numeroViciniCinque);
    fputs(numeroViciniC, file);
    fputs(";", file);

    char numeroTotaleCinque[5];
    sprintf(numeroTotaleCinque, "%i", compNL_five);
    fputs(numeroTotaleCinque, file);
    fputs(";", file);

    for (int i = 1; i < compNL_five + 1; i++) {
        char TempoCinque[5];
        sprintf(TempoCinque, "%.3f", tempoCinque_array[i]);
        fputs(TempoCinque, file);
        fputs(";", file);
        if (i == compNL_five)
            fputs("\n", file);
    }
*/

#ifdef ENABLE_LOG
    // Construct easy pointers array to access the westLog data
    // And record the names of the statistics to prepare a legend later
    westStats* ptr[30];
    compoundWestStats* ptr2 = NULL;

    memset(ptr, 0, 30 * sizeof(westStats*));

    uint8_t idxPtr = 0;

    ptr[idxPtr] = &(westLog.pktTX_KA);
    sprintf(ptr[idxPtr++]->name, "pktTX_KA");
    ptr[idxPtr] = &(westLog.pktTX_HELLO);
    sprintf(ptr[idxPtr++]->name, "pktTX_HELLO");
    ptr[idxPtr] = &(westLog.pktTX_RngPOLL);
    sprintf(ptr[idxPtr++]->name, "pktTX_RngPOLL");
    ptr[idxPtr] = &(westLog.pktTX_RngRESPONSE);
    sprintf(ptr[idxPtr++]->name, "pktTX_RngRESPONSE");
    ptr[idxPtr] = &(westLog.pktTX_RngFINAL);
    sprintf(ptr[idxPtr++]->name, "pktTX_RngFINAL");
    ptr[idxPtr] = &(westLog.pktTX_RngREPORT);
    sprintf(ptr[idxPtr++]->name, "pktTX_RngREPORT");
    ptr[idxPtr] = &(westLog.pktTX_NET);
    sprintf(ptr[idxPtr++]->name, "pktTX_NET");
    ptr[idxPtr] = &(westLog.pktTX_RngKA_NONEIGH);
    sprintf(ptr[idxPtr++]->name, "pktTX_RngKA_NONEIGH");
    ptr[idxPtr] = &(westLog.pktTX_RngHELLO_NONEIGH);
    sprintf(ptr[idxPtr++]->name, "pktTX_RngHELLO_NONEIGH");
    ptr[idxPtr] = &(westLog.pktTX_RngKA);
    sprintf(ptr[idxPtr++]->name, "pktTX_RngKA");
    ptr[idxPtr] = &(westLog.pktTX_RngHELLO);
    sprintf(ptr[idxPtr++]->name, "pktTX_RngHELLO");
    ptr[idxPtr] = &(westLog.noPktTX);
    sprintf(ptr[idxPtr++]->name, "noPktTX");
    ptr[idxPtr] = &(westLog.noRngRESPONSE);
    sprintf(ptr[idxPtr++]->name, "noRngRESPONSE");
    ptr[idxPtr] = &(westLog.pktRX_RngPOLL);
    sprintf(ptr[idxPtr++]->name, "pktRX_RngPOLL");
    ptr[idxPtr] = &(westLog.pktRX_RngRESPONSE);
    sprintf(ptr[idxPtr++]->name, "pktRX_RngRESPONSE");
    ptr[idxPtr] = &(westLog.pktRX_RngFINAL);
    sprintf(ptr[idxPtr++]->name, "pktRX_RngFINAL");
    ptr[idxPtr] = &(westLog.pktRX_RngREPORT);
    sprintf(ptr[idxPtr++]->name, "pktRX_RngREPORT");
    ptr[idxPtr] = &(westLog.pktRX_NET);
    sprintf(ptr[idxPtr++]->name, "pktRX_NET");
    ptr[idxPtr] = &(westLog.rngDuration);
    sprintf(ptr[idxPtr++]->name, "rngDuration");
    ptr[idxPtr] = &(westLog.rngUnfinished);
    sprintf(ptr[idxPtr++]->name, "rngUnfinished");

    ptr2    = &(westLog.validNeighbors);
    sprintf(ptr2->name, "validNeighbors");


    // Get the maximum number of data in order to build a full matrix
    // Check ptrs
    int max = (ptr[0]->howMany) % DIM_LOG;
    for (int i = 1; i < idxPtr; i++){
        if (max < (ptr[i]->howMany) % DIM_LOG)
            max = (ptr[i]->howMany) % DIM_LOG;
    }
    // Check ptr2
    if (max < (ptr2->howMany) % DIM_LOG)
        max = (ptr2->howMany) % DIM_LOG;


    // Record the statistics for this node
    for (int i = 0; i < idxPtr; i++){
        // Skip the empty statistics
        if ( ptr[i]->howMany == 0 )
            continue;

        // Insert the code (index) of the statistic
        fprintf(file, "%d;", i);

        if (ptr[i]->howMany < DIM_LOG) {
            // Insert how many values of this statistic
            fprintf(file, "%d;", ptr[i]->howMany);

            // Insert the values
            for (int j = 0; j < ptr[i]->howMany; j++){
                fprintf(file, "%f;", ptr[i]->timestamp[j]);
            }

            // Fill missing values to max and set them to nan
            for (int j = ptr[i]->howMany; j < max; j++){
                fputs("nan;", file);
            }
        } else {
            // Insert how many values of this statistic
            fprintf(file, "%d;", DIM_LOG);

            // Insert the values
            for (int j = (ptr[i]->howMany-DIM_LOG); j < (ptr[i]->howMany); j++){
                fprintf(file, "%f;", ptr[i]->timestamp[j % DIM_LOG]);
            }
        }

        // New line = new statistic
        fputs("\n", file);  // new line = new statistic
    }

    // Add the trace of valid neighbors, if present
    if ( ptr2->howMany > 0 ) {
        // Insert the code (index) of the statistic (neighbor timestamp)
        fprintf(file, "%d;", idxPtr);

        if (ptr2->howMany < DIM_LOG) {
            // Insert how many values of this statistic
            fprintf(file, "%d;", ptr2->howMany);

            // Insert the values
            for (int j = 0; j < ptr2->howMany; j++){
                fprintf(file, "%f;", ptr2->stats[j].timestamp);
            }

            // Fill missing values to max and set them to nan
            for (int j = ptr2->howMany; j < max; j++){
                fputs("nan;", file);
            }
        } else {
            // Insert how many values of this statistic
            fprintf(file, "%d;", DIM_LOG);

            // Insert the values
            for (int j = (ptr2->howMany-DIM_LOG); j < (ptr2->howMany); j++){
                fprintf(file, "%f;", ptr2->stats[j % DIM_LOG].timestamp);
            }
        }

        // New line = new statistic
        fputs("\n", file);  // new line = new statistic

        // Insert the code (index) of the statistic (neighbor value)
        fprintf(file, "%d;", idxPtr+1);

        if (ptr2->howMany < DIM_LOG) {
            // Insert how many values of this statistic
            fprintf(file, "%d;", ptr2->howMany);

            // Insert the values
            for (int j = 0; j < ptr2->howMany; j++){
                fprintf(file, "%d;", ptr2->stats[j].value);
            }

            // Fill missing values to max and set them to nan
            for (int j = ptr2->howMany; j < max; j++){
                fputs("nan;", file);
            }
        } else {
            // Insert how many values of this statistic
            fprintf(file, "%d;", DIM_LOG);

            // Insert the values
            for (int j = (ptr2->howMany-DIM_LOG); j < (ptr2->howMany); j++){
                fprintf(file, "%d;", ptr2->stats[j % DIM_LOG].value);
            }
        }

        // New line = new statistic
        fputs("\n", file);  // new line = new statistic
    }

    // Visualize the value of the logOption
    fputs("%\n%\n%logOptions:\n", file);
    uint16_t maskOptions[LOG_NUM_MASKS] = {LOG_MASK_TX, LOG_MASK_RX,
            LOG_MASK_RNGERR, LOG_MASK_RNGNOTFOUND, LOG_MASK_NODC, LOG_MASK_RNGDUR, LOG_MASK_VALIDNEIGH};
    char maskNames[LOG_NUM_MASKS][50];

    sprintf(&(maskNames[0][0]), "TX");
    sprintf(&(maskNames[1][0]), "RX");
    sprintf(&(maskNames[2][0]), "RNGERR");
    sprintf(&(maskNames[3][0]), "RNG_NOT_FOUND");
    sprintf(&(maskNames[4][0]), "NODC");
    sprintf(&(maskNames[5][0]), "RNGDUR");
    sprintf(&(maskNames[6][0]), "VALIDNEIGH");

    int idxMask = 0;
    while (logOption != 0 && idxMask < LOG_NUM_MASKS) {
        if (LOG_CHECK_OPTION(logOption,maskOptions[idxMask])) {
            fputs("%", file);   // insert MATLAB comment symbol
            fprintf(file, "%s\n", maskNames[idxMask]);
            logOption &= ~(LOG_BUILD_MASK(maskOptions[idxMask]));   // Clear that bit
        }
        idxMask++;
    }

    // Create in the output file a legend to associate the code numbers to meanings
    fputs("%\n%\n%Legend:\n", file);
    fputs("%StatCode;Meaning\n", file);
    for (int i = 0; i < idxPtr; i++) {
        fputs("%", file);  // insert MATLAB comment symbol
        fprintf(file, "%d;%s\n", i, ptr[i]->name);
    }
    fputs("%", file);  // insert MATLAB comment symbol
    fprintf(file, "%d;%s_time\n", idxPtr, ptr2->name);
    fputs("%", file);  // insert MATLAB comment symbol
    fprintf(file, "%d;%s_value\n", idxPtr+1, ptr2->name);

#endif

    fclose(file);

#if 0
    fileTwo = fopen(fileNameTwo, "a+");

    char myNodeIdStr[5];
    sprintf(myNodeIdStr, "%i", nodeID);
    fputs(myNodeIdStr, fileTwo);
    fputs(";", fileTwo);

    for (int i = 1; i < countInertialTimes + 1; i++) {
        char Tempo[5];
        sprintf(Tempo, "%.3f", inertialTimes[i]);
        fputs(Tempo, fileTwo);
        fputs(";", fileTwo);
        char Neighbors[5];
        sprintf(Neighbors, "%i", numNeighbors[i]);
        fputs(Neighbors, fileTwo);
        fputs(";", fileTwo);
        if (i == countInertialTimes)
            fputs("\n", fileTwo);
    }

    fclose(fileTwo);
#endif
}

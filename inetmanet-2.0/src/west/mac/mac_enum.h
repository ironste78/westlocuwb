// MAC enumeration and packet types

#ifndef MAC_ENUM_H
#define MAC_ENUM_H

// Packet types
enum
{
    PKT_KEEP_ALIVE = 0x01,
    PKT_HELLO = 0x02,
    PKT_RNG_POLL = 0x21,
    PKT_RNG_RESPONSE = 0x10,
    PKT_RNG_FINAL_MSG = 0x29,
    PKT_RNG_REPORT_MSG = 0x2A,
    PKT_NET_DATA = 0xAA
};

// Ranging Code
enum
{
    RNG_UNSET = 0x00,
    RNG_DONE = 0x01,
    RNG_PENDING = 0x02
};

// Transmission Error Codes
enum
{
    TX_SUCCESS      = 0x00,
    TX_T_ON         = 0x01,
    TX_NO_RNG       = 0x02,
    TX_NO_DATA      = 0x03,
    TX_GENERIC_ERROR   = 0x04
};


#define TOKEN_CODE   0x80

// Size of WestFrame. To be updated if packet changes in WestFrame.msg!!
#define WEST_FRAME_SIZE 11

#endif
